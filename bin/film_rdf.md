# RDF structure for pm20 films

RDF production to become part of merge_film_ids.pl? YES

## classes and url patterns

* Pm20FilmSet `https://pm20.zbw.eu/film/h{1|2}`
* Pm20FilmSubset `https://pm20.zbw.eu/film/h{1|2}/{co|pe|sh|wa}`
* Pm20Film `https://pm20.zbw.eu/film/h{1|2}/{co|pe|sh|wa}/{filmid}`
* Pm20FilmItem `https://pm20.zbw.eu/film/h{1|2}/{co|pe|sh|wa}/{filmid}/{imgid}(/{R|L})?
* ?? Pm20Holding  (h|k)
* ?? Pm20Collection
* ?? Pm20Filming 

## properties

### has part hierarchy

* {filmset} dct:hasPart {filmsubset} dct:hasPart {film} dct:hasPart {filmitem}
* materialize dct:isPartOf? or only this?

### film (from filmlists)

* zbwext:firstImage
* zbwext:lastImage
* zbwext:totalImageCount
* online (as exclusion criterium)

### film section/item (from zotero)

* zbwext:country (from geo/id)
* dct:title
* schema:startDate ("start_date") - dct:temporal for folder
* zbwext:wdIdentifier ("qid")
* zbwext:totalImageCount (scanned images, also for film, folder, category) ("number_of_images")
* ??? zbwext:freeImageCount
* skos:notation ("signature_string")
* schema:about (for reference to pm20 companies or folders)

* (zbwext:belongsToFolder) -> schema:about
* (zbwext:belongsToCategory) -> zbwext:(country/subject/ware)

## todo

        "valid_sig": 1,   #evtl. neues property, angelehnt an disco:isValid
        "direct_pm20id": "",
        "pm20Id": "co/047669",
        "lr": "L"

