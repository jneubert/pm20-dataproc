#!/bin/perl
# nbt, 31.1.2018

# traverses database (for co/pe) or image share (for sh/wa) results and create
# turtle data

use strict;
use warnings;
use lib '../lib';
use utf8;

use Data::Dumper;
use Date::Format;
use Date::Parse;
use JSON;
use Path::Tiny;
use ZBW::Ifis::Schema;
use ZBW::PM20x::Film;

#$Data::Dumper::Sortkeys = 1;
binmode( STDOUT, ":utf8" );
binmode( STDERR, ":utf8" );

##my @holdings = qw/ co /;
my @holdings        = qw/ co pe sh wa /;
my $folder_root_uri = 'https://pm20.zbw.eu/folder/';
my $old_root_uri    = 'http://purl.org/pressemappe20/folder/';
my $instname_root   = 'http://zbw.eu/beta/instname/';
my $dfgview_root    = 'https://pm20.zbw.eu/dfgview/';
my $imagedata_root  = path('../data/imagedata');
my $docdata_root    = path('../data/docdata');
my $folderdata_root = path('../data/folderdata');
my $family_re       = qr/.*? <Familie[>,]/;
my $countrycode_re  = qr/^[2-6]([A-Z]{2})$/;

# metadata/structures for holdings
my %holding_struct = (
  pe => {
    rdf_type  => 'zbwext:PersonFolder',
    record    => 'Person',
    id_fld    => 'nr_person',
    label_fld => 'name_person',
    gnd_fld   => 'pnd_id',
    clip_fld  => 'pers_pd_ausschn',
  },
  co => {
    rdf_type     => 'zbwext:CompanyFolder',
    record       => 'Institution',
    id_fld       => 'nr_inst',
    label_fld    => 'inst_name',
    gnd_fld      => 'gkd_id',
    clip_fld     => 'inst_pd_ausschn',
    rdf_lifespan => {
      fromTo => 'zbwext:fromTo',
      from   => 'schema:foundingDate',
      to     => 'schema:dissolutionDate',
    },
  },
  sh => {
    rdf_type => 'zbwext:SubjectFolder',
    record   => 'Klassifikator',
  },
  wa => {
    rdf_type => 'zbwext:WareFolder',
    record   => 'Klassifikator',
  },
);

# DB access
our $debug = 0;
my $schema = ZBW::Ifis::Schema->get_schema();

# print turtle header
my $prefixes =
    "\@prefix dc: <http://purl.org/dc/elements/1.1/> .\n"
  . "\@prefix dct: <http://purl.org/dc/terms/> .\n"
  . "\@prefix esco: <http://data.europa.eu/esco/model#> .\n"
  . "\@prefix frapo: <http://purl.org/cerif/frapo/> .\n"
  . "\@prefix gn: <http://www.geonames.org/ontology#> .\n"
  . "\@prefix gndo: <https://d-nb.info/standards/elementset/gnd#> .\n"
  . "\@prefix pm20geo: <https://pm20.zbw.eu/category/geo/i/> .\n"
  . "\@prefix pm20ware: <https://pm20.zbw.eu/category/ware/i/> .\n"
  . "\@prefix pm20subject: <https://pm20.zbw.eu/category/subject/i/> .\n"
  . "\@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
  . "\@prefix rdfs: <https://d-nb.info/standards/elementset/gnd#gndIdentifier> .\n"
  . "\@prefix schema: <http://schema.org/> .\n"
  . "\@prefix skos: <http://www.w3.org/2004/02/skos/core#> .\n"
  . "\@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n"
  . "\@prefix zbwext: <http://zbw.eu/namespaces/zbw-extensions/> .\n";
print $prefixes;

# read image files as basis for all output
foreach my $holding (@holdings) {

  my %struct = %{ $holding_struct{$holding} };
  my $docdata_ref =
    decode_json( $docdata_root->child("${holding}_docdata.json")->slurp );

  # iterate over database
  # for holdings which are represented as database records
  if ( $holding eq 'co' or $holding eq 'pe' ) {
    my $clip_fld = $struct{clip_fld};
    my $main_rs  = $schema->resultset( $struct{record} );
    while ( my $rec = $main_rs->next ) {
      my $rel_recs;

      # IDs
      my $id  = sprintf( "%06d", $rec->id );
      my $fid = "$holding/$id";
      my $uri = "<$folder_root_uri$fid>";

      # last modified data
      my $time     = str2time( $rec->s_timestamp );
      my $modified = time2str( '%Y-%m-%d', $time );

      # skip records without any clippings
      if ( exists $docdata_ref->{$id} ) {
        ## folders without any document references in the database
        ## are relevant when documents exist on disk
      } elsif ( defined( $rec->$clip_fld ) ) {
        next if ( $rec->$clip_fld =~ m/kein material/i );
        next if ( $rec->$clip_fld =~ m/kein dossier/i );
      } elsif ( $holding eq 'co' and defined( $rec->inst_pd_bericht ) ) {
        ## folders only with reports are relevant, too
      } elsif (
        $holding eq 'co'
        and (ZBW::PM20x::Film->foldersections( $fid, 1 )
          or ZBW::PM20x::Film->foldersections( $fid, 2 ) )
        )
      {
        ## folders with material on film only also
      } else {
        next;
      }

      # classes and  and labels
      my $label_fld = $struct{label_fld};
      my $label     = trim( $rec->$label_fld );
      my $adjusted_label;
      if ( $holding eq 'co' ) {
        $adjusted_label = adjust_co_label($label);
        if ( $adjusted_label ne $label ) {
          ##warn "$label -> $adjusted_label\n";
        }
        $adjusted_label = quote($adjusted_label);
      }
      $label = quote($label);

      print "\n$uri skos:prefLabel $label .\n";
      print "$uri rdf:type zbwext:Pm20Folder .\n";
      print "$uri rdf:type $struct{rdf_type} .\n";
      print "$uri dct:identifier \"$fid\" .\n";
      if ( $holding eq 'co' ) {
        print "$uri zbwext:adjustedLabel $adjusted_label .\n";
      }
      print "$uri dct:modified \"$modified\" .\n";

      # GND id
      my $gnd_fld = $struct{gnd_fld};
      if ( defined $rec->$gnd_fld ) {
        print "$uri gndo:gndIdentifier \"" . trim( $rec->$gnd_fld ) . "\" .\n";
      }

      # temporal coverage
      if ( defined( my $field = $rec->$clip_fld ) ) {
        $field = trim($field);
        print "$uri dct:temporal \"$field\" .\n";
      }

      ### persons ###

      if ( $holding eq 'pe' ) {

        # mark families
        if ( $rec->name_person =~ $family_re ) {
          print "$uri a gndo:Family .\n";
        }

        # life span of persons
        if ( defined( my $timespan = $rec->pers_lebdat ) ) {
          print "$uri gndo:dateOfBirthAndDeath \"$timespan\" .\n";
        }
        if ( defined( my $date = $rec->geb_dat ) ) {
          print "$uri schema:birthDate \"$date\" .\n";
        }
        if ( defined( my $date = $rec->tod_dat ) ) {
          print "$uri schema:deathDate \"$date\" .\n";
        }

        # occupation
        if ( defined( my $text = $rec->pers_beruf ) ) {
          $text =~ tr/"/'/;
          $text = trim($text);
          print "$uri schema:hasOccupation \"$text\" .\n";
        }

        # nationality
        # TODO entity
        if ( defined( $rec->nr_klass ) ) {
          print "$uri schema:nationality \""
            . $rec->nr_klass_relation->name_term
            . "\"\@de .\n";
          print "$uri schema:nationality \""
            . $rec->nr_klass_relation->name_term_en
            . "\"\@en .\n";
        }

        # activity
        # TODO entities
        foreach my $wirk ( $rec->pers_wirk_relations ) {
          my $location = '"'
            . $wirk->nr_klass_relation->name_term
            . '"@de,"'
            . $wirk->nr_klass_relation->name_term_en . '"@en';
          my $about = '"'
            . $wirk->nr_wirkbereich_relation->wirkbereich_dt
            . '"@de,"'
            . $wirk->nr_wirkbereich_relation->wirkbereich_en . '"@en';
          my $from_to =
            defined( $wirk->wirk_von_bis )
            ? '"' . $wirk->wirk_von_bis . '"'
            : undef;
          my %activity_struct = (
            "schema:location" => $location,
            "schema:about"    => $about,
            "zbwext:fromTo"   => $from_to,
          );

          my @structs;
          foreach my $struct ( keys %activity_struct ) {
            next unless defined $activity_struct{$struct};
            push( @structs, " $struct $activity_struct{$struct} " );
          }
          next unless ( scalar(@structs) gt 0 );
          print "$uri zbwext:activity [ " . join( ';', @structs ) . " ] .\n";
        }

        # structured fields

        # related person
        my $rels = $rec->person_relations;
        while ( my $rel_rec = $rels->next ) {

          my $rel_fid  = "pe/" . sprintf( "%06d", $rel_rec->id );
          my $rel_url  = "$folder_root_uri$rel_fid";
          my $rel_name = quote( trim( $rel_rec->name_person ) );

          # special case family relations
          if ( $rel_name =~ $family_re ) {
            print "$uri dct:isPartOf [ schema:url '$rel_url'; "
              . "schema:name $rel_name ] .\n";
            print "<$rel_url> dct:hasPart [ schema:url '$folder_root_uri$fid'; "
              . "schema:name $label ] .\n";
          } else {
            print "$uri gndo:relatedPerson [ schema:url '$rel_url'; "
              . "schema:name $rel_name ] .\n";
            print "<$rel_url> gndo:relatedPerson [ "
              . "schema:url '$folder_root_uri$fid'; "
              . "schema:name $label ] .\n";
          }
        }

        # note for pe
        $rel_recs = $rec->pers_text_relations;
        while ( my $rel_rec = $rel_recs->next ) {

          # The record contains two fields, pers_text and text.
          # The content of text is displayed in the Pressemappen application.
          # pers_text seems to be a prior concatenation of beruf and text.
          my $text = $rel_rec->text;

          # skip empty text records
          next unless $text;

          # add a trailing dot to avoid possible clash with trailng single
          # qoute
          if ( not $text =~ m/\.$/ ) {
            $text = trim($text) . '.';
          }

          $text = quote($text);

          if ( $rel_rec->nr_texttyp == 11 ) {
            print "$uri skos:note $text .\n";
          } else {
            ## skip links or unused types
            next;
          }
        }
      }

      ### organizations ###

      if ( $holding eq 'co' ) {

        # life span of organizations
        if ( my $timespan = $rec->inst_anf_ende ) {
          print "$uri zbwext:fromTo \"$timespan\" .\n";
          if ( $timespan =~ m/^(\d\d\d\d)?(\s*\-\s*)?(\d\d\d\d)?\s*$/ ) {
            if ($1) {
              print "$uri schema:foundingDate \"$1\" .\n";
            }
            if ($3) {
              print "$uri schema:dissolutionDate\"$3\" .\n";
            }
          } else {
            warn "Irregular inst_anf_ende in $fid: $timespan\n";
          }
        }

        # organization type
        # TODO entity
        $rel_recs = $rec->insttyp_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          print "$uri dc:type \"" . $rel_rec->insttyp_dt . "\"\@de .\n";
          print "$uri dc:type \"" . $rel_rec->insttyp_en . "\"\@en .\n";
        }

        # company notation
        # TODO firmsign?
        if ( my $nta = $rec->firmsiga ) {

          # normalize country notation
          my $nta_qr = qr/^([A-Z]) (.+)$/;
          if ( $nta =~ m/$nta_qr/ ) {
            $nta =~ s/$nta_qr/$1$2/;
          } else {
            warn "Irregular signature in $fid: $nta\n";
          }
          print "$uri skos:notation \"$nta\"^^xsd:string .\n";
        }

        # annual reports
        if ( my $txt = $rec->inst_pd_bericht ) {
          $txt = quote("GB: $txt");
          print "$uri dct:temporal $txt .\n";
        }

        # note for co
        # two candidates from inst_text relation:
        # - inst_text (varchar 2000)
        # - text (text)
        # It is not clear, which of the fields contains more accurate and more
        # recent information
        # For textyp = 2, text has 2056 occurences, while inst_text has 463.
        # For now, use text, because 1) more, and 2) some inst_text values are
        # truncated.
        $rel_recs = $rec->inst_text_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          my $text = $rel_rec->text;

          # skip empty text records
          next unless $text;

          # add a trailing dot to avoid possible clash with trailng single
          # qoute
          if ( not $text =~ m/\.$/ ) {
            $text = trim($text) . '.';
          }

          $text = quote($text);

          my $texttyp = $rel_rec->nr_texttyp;
          if ( $texttyp == 4 ) {
            print "$uri skos:editorialNote $text .\n";
          } elsif ( $texttyp == 2 ) {
            print "$uri skos:note $text .\n";
          } else {
            ## skip links or unused types
          }
        }

        # Wikipedia links
        $rel_recs = $rec->inst_text_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          my $wp_url = $rel_rec->url;

          # skip empty text records
          next unless $wp_url;

          # skip wrong text tyes
          my $texttyp = $rel_rec->nr_texttyp;
          next unless ( $texttyp == 6 );

          # skip links if not to Wikipedia
          next unless ( $wp_url =~ m/wikipedia\.org/ );

          # format link matching to wd
          $wp_url =~ s/^http:/https:/;
          $wp_url =~ s/%28/\(/g;
          $wp_url =~ s/%29/\)/g;
          $wp_url =~ s/'/%27/g;

          print "$uri zbwext:wpLinkOrig <$wp_url> . \n";
        }

        # industry sector (SK)
        # TODO entity
        $rel_recs = $rec->instklass_sk_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          print "$uri schema:industry \"" . $rel_rec->name_term . "\"\@de .\n";
          print "$uri schema:industry \""
            . $rel_rec->name_term_en
            . "\"\@en .\n";
          ##print "$uri schema:industry \"" . $rel_rec->klass_not . "\"^^xsd:string .\n";
        }

        # industry sector (NACE2)
        # TODO entity
        $rel_recs = $rec->instklass_na_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          my $code = $rel_rec->klass_not;

          # NACE2 codes in IFIS start with a dot
          if ( $code =~ m/^\.(.*)$/ ) {
            $code = trim($1);
          } else {
            warn "Irregular NACE code in $fid: ", $rel_rec->klass_not, "\n";
            next;
          }
          print "$uri esco:hasNACECode \"$code\"^^xsd:string .\n";
        }

        # location
        # TODO entity
        $rel_recs = $rec->instklass_gk_relations;
        while ( my $rel_rec = $rel_recs->next ) {
          my $loc_label_de = $rel_rec->name_term;
          print "$uri schema:location \"$loc_label_de\"\@de .\n";

          # use German label if no English label exists
          my $loc_label_en = $rel_rec->name_term_en || $rel_rec->name_term;
          print "$uri schema:location \"$loc_label_en\"\@en .\n";

          # Geonames ids can be extrated, as they are used as notations for
          # minor features (apparently, not for countries and states!)
          my $gn_id;
          if ( $rel_rec->klass_not =~ m/^X(\d+)\s*$/ ) {
            $gn_id = $1;
            print "$uri gn:locatedIn "
              . "[ schema:url 'http://sws.geonames.org/$gn_id' ] .\n";
          }

          # has the location a country code?
          my $country_missing = 1;
          if ( $rel_rec->klass_not =~ $countrycode_re ) {
            print "$uri frapo:hasCountryCode \"$1\" .\n";
            $country_missing = 0;
          }

          # broader location
          my $broader_recs = $rel_rec->klass_broader_relations;
          while ( my $broader_rec = $broader_recs->next ) {
            my $broader_label_de = $broader_rec->name_term;
            print "$uri zbwext:broaderLocation \"$broader_label_de\"\@de .\n";

            # use German label if no English label exists
            my $broader_label_en =
              $broader_rec->name_term_en || $broader_rec->name_term;
            print "$uri zbwext:broaderLocation \"$broader_label_en\"\@en .\n";

            next unless $country_missing;

            # has the broader location a country notation?
            if ( $broader_rec->klass_not =~ $countrycode_re ) {
              print "$uri frapo:hasCountryCode \"$1\" .\n";
            } else {
              my $more_broader_recs = $broader_rec->klass_broader_relations;
              while ( my $more_broader_rec = $more_broader_recs->next ) {

                # country notation here?
                if ( $more_broader_rec->klass_not =~ $countrycode_re ) {
                  print "$uri frapo:hasCountryCode \"$1\" .\n";
                } else {
                  my $even_broader_recs =
                    $more_broader_rec->klass_broader_relations;
                  while ( my $even_broader_rec = $even_broader_recs->next ) {

                    # country notation here?
                    if ( $even_broader_rec->klass_not =~ $countrycode_re ) {
                      print "$uri frapo:hasCountryCode \"$1\" .\n";
                    }
                  }
                }
              }
            }
          }
        }

        # inter-institution relations
        # TODO entities
        my $rels = $rec->inst_rel_nr_inst_relations;
        while ( my $rel = $rels->next ) {

          my $rel_rec = $rel->ins_nr_inst_relation;
          my $rel_url =
            "${folder_root_uri}co/" . sprintf( "%06d", $rel_rec->id );
          my $rel_name = quote( trim( $rel_rec->inst_name ) );

          # add relations and reverse relations (created here) according to
          # different types of relations
          my ( $rel_prop, $rev_prop );
          my $reltyp = $rel->inst_reltyp;

          # successor
          if ( $reltyp == 5 ) {
            $rel_prop = 'gndo:succeedingCorporateBody';
            $rev_prop = 'gndo:precedingCorporateBody';

            # sub org
          } elsif ( $reltyp == 6 ) {
            $rel_prop = 'schema:subOrganization';
            $rev_prop = 'schema:parentOrganization';

            # related
          } elsif ( $reltyp == 7 ) {
            $rel_prop = 'gndo:relatedCorporateBody';
            $rev_prop = 'gndo:relatedCorporateBody';
          } else {
            die "unknown $reltyp\n";
          }

          print "$uri $rel_prop [ schema:url '$rel_url' ;"
            . " schema:name $rel_name ] .\n";
          ##print "$uri $rel_prop <$rel_url> .\n";
          print "<$rel_url> $rev_prop [ schema:url '$folder_root_uri$fid' ;"
            . " schema:name $label ] .\n";
          ##print "<$rel_url> $rel_prop $uri .\n";
        }

        # member roles
        $rels = $rec->inst_funkpers_relations;
        while ( my $rel = $rels->next ) {
          my $rel_rec = $rel->nr_person_relation;
          my $rel_url =
            "${folder_root_uri}pe/" . sprintf( "%06d", $rel_rec->id );
          my $rel_name = quote( trim( $rel_rec->name_person ) );

          # add relations and reverse relations (created here) according to
          # different types of relations
          my $reltyp = $rel->nr_funkpers_relation;
          my $from_to =
            $rel->dauer ? 'zbwext:fromTo "' . $rel->dauer . '" ; ' : '';
          print "$uri schema:member [ schema:url '$rel_url' ; "
            . "schema:roleName \""
            . $reltyp->funkpers_dt
            . '"@de ; '
            . "schema:roleName \""
            . $reltyp->funkpers_en
            . '"@en ; '
            . $from_to
            . "schema:name $rel_name ] .\n";
          print
            "<$rel_url> schema:memberOf [ schema:url '$folder_root_uri$fid' ; "
            . "schema:roleName \""
            . $reltyp->funkpers_dt
            . '"@de ; '
            . "schema:roleName \""
            . $reltyp->funkpers_en
            . '"@en ; '
            . $from_to
            . "schema:name $label ] .\n";
        }

        # synonyms and subsumed institutions
        $rels = $rec->inst_syn_relations;
        while ( my $rel = $rels->next ) {
          my $instname_uri = "<$instname_root" . $rel->nr_inst_syn . ">";
          print "$uri zbwext:includesInstitutionNamed $instname_uri .\n";
          print "$instname_uri schema:name " . quote( $rel->inst_syn ) . " .\n";
          if ( $rel->inst_syn_von_bis ) {
            print "$instname_uri skos:note "
              . quote( $rel->inst_syn_von_bis ) . " .\n";
          }

          # if a gnd identifier exists for the institution name, it is probably
          # not a synonym - else, we assume it may be / have been at some time
          if ( my $gnd = $rel->inst_syn_gkd ) {
            print "$instname_uri gndo:gndIdentifier \"$gnd\" .\n";
          } else {
            print "$uri skos:altLabel "
              . quote( trim( $rel->inst_syn ) ) . " .\n";
            print "$uri zbwext:adjustedAltLabel "
              . quote( adjust_co_label( trim( $rel->inst_syn ) ) ) . " .\n";
          }
        }

      }

      ### evaluate documents data for the folder ###

      if ( exists $docdata_ref->{$id} ) {
        add_docdata( $holding, $uri, $docdata_ref->{$id} );

        # add dfgviewer url if documents exist
        print "$uri zbwext:viewUrl <$dfgview_root$fid> .\n";
      }
    }
  }

  if ( $holding eq 'sh' or $holding eq 'wa' ) {

    # use ifis for label lookup
    my $main_rs = $schema->resultset( $struct{record} );

    # iterate over folder ids from document directories
    # (this information cannot be derived from the ifis database!)
    foreach my $id ( keys %{$docdata_ref} ) {
      my $fid = "$holding/$id";
      my $uri = "<$folder_root_uri$fid>";
      $id =~ m/(\d{6}),(\d{6})/;
      my $id1 = $1;
      my $id2 = $2 or die "irregular id: $id\n";

      # lookup according records
      my $rec1 = $main_rs->find($id1);
      my $rec2 = $main_rs->find($id2);

      # general properties
      print "\n$uri rdf:type zbwext:Pm20Folder .\n";
      print "$uri rdf:type $struct{rdf_type} .\n";
      print "$uri dct:identifier \"$fid\" .\n";
      my $label_de = quote( $rec1->name_term . ' : ' . $rec2->name_term );
      print "$uri skos:prefLabel $label_de\@de .\n";
      my $label_en = quote( $rec1->name_term_en . ' : ' . $rec2->name_term_en );
      print "$uri skos:prefLabel $label_en\@en .\n";

      ### subjects ###

      if ( $holding eq 'sh' ) {

        # links to subject and geo classification
        print "$uri zbwext:country pm20geo:$id1 .\n";
        print "$uri zbwext:subject pm20subject:$id2 .\n";
      }

      ### wares ###

      if ( $holding eq 'wa' ) {

        # links to subject and ware classification
        print "$uri zbwext:country pm20geo:$id2 .\n";
        print "$uri zbwext:ware pm20ware:$id1 .\n";
      }

      ### evaluate documents data for the folder ###

      if ( exists $docdata_ref->{$id} ) {
        add_docdata( $holding, $uri, $docdata_ref->{$id} );

        # add dfgviewer url if documents exist
        print "$uri zbwext:viewUrl <$dfgview_root$fid> .\n";
      }
    }
  }
}

#####################

sub trim {
  my $text = shift or die "param missing\n";

  # remove trailing line break
  $text =~ s/\n+$//sm;

  # remove trailing white space
  $text =~ s/\s+$//;

  # remove leading white space
  $text =~ s/^\s+//;

  # remove multiple inline whitespace
  $text =~ s/\s+/ /g;

  return $text;
}

sub quote {
  my $text = shift or die "param missing\n";

  # save quoting for turtle
  my $q = "'''";

  # error with with trailing single quote, so:
  if ( $text =~ m/'$/ ) {
    $q = '"""';
  }

  return $q . $text . $q;
}

sub add_docdata {
  my $holding = shift;
  my $uri     = shift;
  my %docs    = %{ (shift) };

  # document counts
  print "$uri zbwext:totalDocCount " . scalar( keys %{ $docs{info} } ) . " .\n";
  print "$uri zbwext:freeDocCount " . scalar( keys %{ $docs{free} } ) . " .\n";

  # document types - currently only business reports
  if ( $holding eq 'co' ) {
    my $report_count = 0;
    foreach my $doc ( sort keys %{ $docs{info} } ) {
      if (  $docs{info}{$doc}{con}{type}
        and $docs{info}{$doc}{con}{type} eq 'G' )
      {
        $report_count++;
      }
    }
    print "$uri zbwext:reportCount $report_count .\n";
  }
}

sub adjust_co_label {
  my $label = shift or die "co label missing";

  ## remove location in the end, in parenthesis - too much variation
  ##my @locations = qw/ London Glasgow Liverpool Birmingham Manchester Edinburgh Bristol Berlin Grimethorpe Budweis Swindon /;
  ##push(@locations, 'Liverpool, Merseyside', 'Port Talbot');

  # just assume that every end expression in parens is a location, and remove
  $label =~ s/(.*) \(.*?\)$/$1/g;

  # cut off legal form

  # keep special case with ending "& Co"
  if ( $label =~ m/(.*) (\&|and) (Co|Cie)(\.)?$/ ) {
    ## do nothing
  }

  # "...s AG"
  elsif ( $label =~ m/s AG$/ ) {
    ## do nothing
  }

  # replace ending AG in "& Co AG" etc.
  elsif ( $label =~ m/(.* \& Co(\.)?) AG$/ ) {
    $label = $1;
  }

  # replace ending Ltd in "and Company, Ltd", "& Co.,Ltd" etc.
  elsif ( $label =~ m/(.* (\&|and) (Co|Cie|Company)(\.)?)\s?,?\sLtd(\.)?$/ ) {
    $label = $1;
  } else {
    $label =~ s/(.*) AG \& Co$/$1/g;
    $label =~ s/(.*) A\.G\.$/$1/g;
    $label =~ s/(.*) AG$/$1/g;
    $label =~ s/(.*) AS$/$1/g;
    $label =~ s/(.*) A\/S$/$1/g;
    $label =~ s/(.*) AB$/$1/g;
    $label =~ s/(.*) Aktiengesellschaft$/$1/g;
    $label =~ s/(.*) BV$/$1/g;
    $label =~ s/(.*) EP$/$1/g;
    $label =~ s/(.*) EP$/$1/g;
    $label =~ s/(.*) SARL$/$1/g;
    $label =~ s/(.*) oHG$/$1/g;
    $label =~ s/(.*) KG$/$1/g;
    $label =~ s/(.*) Co Inc\.$/$1/g;
    $label =~ s/(.*) Co\. Ltd\.$/$1/g;
    $label =~ s/(.*) Ltd\.$/$1/g;
    $label =~ s/(.*) Ltd$/$1/g;
    $label =~ s/(.*) Limited$/$1/g;
    $label =~ s/(.*) \& Co\.KG$/$1/ig;
    $label =~ s/(.*) Co\.$/$1/ig;
    $label =~ s/(.*) Co$/$1/ig;
    $label =~ s/(.*) Corpi\.$/$1/ig;
    $label =~ s/(.*) Corp\.$/$1/ig;
    $label =~ s/(.*) Corp$/$1/ig;
    $label =~ s/(.*) eG$/$1/ig;
    $label =~ s/(.*) mbh$/$1/ig;
    $label =~ s/(.*) Gmbh$/$1/ig;
    $label =~ s/(.*) eGmbh$/$1/ig;
    $label =~ s/(.*) Gmbh \&$/$1/ig;
    $label =~ s/(.*) e\.V\.$/$1/ig;
    $label =~ s/(.*) eV$/$1/g;
    $label =~ s/(.*) Inc\.$/$1/g;
    $label =~ s/(.*) Inc$/$1/g;
    $label =~ s/(.*) plc\.$/$1/g;
    $label =~ s/(.*) plc$/$1/g;
    $label =~ s/(.*) SA$/$1/g;
    $label =~ s/(.*) S\.A\.$/$1/g;
    $label =~ s/(.*) NV$/$1/g;
    $label =~ s/(.*) N\.V\.$/$1/g;
    $label =~ s/(.*) SpA$/$1/g;
    $label =~ s/(.*) \(GmbH \& Co\)/$1/g;
    $label =~ s/(.*) \(GmbH \& Co\)/$1/g;
    $label =~ s/(.*) Co\.,$/$1/g;

    # prefix (dutch)
    $label =~ s/^N\.V\. (.*)$/$1/g;
    $label =~ s/^NV (.*)$/$1/g;

  }

  # rests
  $label =~ s/(.*),$/$1/g;

  return $label;
}
