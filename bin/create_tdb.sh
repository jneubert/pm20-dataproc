#!/bin/bash
# nbt, 28.8.09

# create a tdb and text index for a dataset

# with the "reindex" parameter, only the text index is rebuilt

# ALWAYS create dataset in temp area (in parallel to the existing database)
#
# In case of reindex,
# - deactivate dataset (#service) in config.ttl
# - restart fuseki
# - run script as root (file perms!)
# - reactivate dataset
# - restart fuseki

export JENAROOT="/opt/jena"
export FUSEKI_HOME="/opt/fuseki"
PATH=$JENAROOT/bin:$PATH
DEFAULT_TDB_LOADER=tdb2.tdbloader
TDB_LOADER=$DEFAULT_TDB_LOADER
LOADER_OPTS=""
TEXT_INDEXER=jena.textindexer

# Java options
# -d64 necessary for 64bit os
export JAVA_OPTS="-d64 -Xmx1g"
if [[ "nbt3 nbt4" =~ `hostname -s` ]]; then
  export JAVA_OPTS="-d64 -Xmx4G"
fi
# on hal-fuseki container
if [[ "e6810f891672" =~ `hostname -s` ]]; then
  if [ "$1" != "wdfull" ]; then
    JAVA_OPTS="-d64 -Xmx12G"
    LOADER_OPTS="--loader=parallel"
  else
    TDB_LOADER=tdb2.xloader
    LOADER_OPTS="--threads=128"
    # fixed setting of -Xmx4G -
    # JAVA_OPTS and JVM_ARGS (!) are not interpreted by tdb2.xloader
    #TMPDIR=/dev/shm/sort_tmp
    #TMPDIR=/zbw/tmp
    # mkdir -p $TMPDIR
    ## using tmpdir on the same disk does not improve anything
    ##export XLOADER_OPTS="--threads=48 --tmpdir=$TMPDIR"
  fi
fi

# since output is piped to tee, the return value of the originating program
# has to be considered
set -o pipefail

# user, for file ownership on re-indexed db
USER="fuseki:fuseki"

# check number of arguments
if [ $# -lt 4 ]
then
  echo "Usage: `basename $0` direct?|reindex? {dataset} default|{graph} reindex|{datafile} ... (relative or ablsolute paths)"
  exit 1
fi

# first param perhaps a command?
ACTION=create
if [ "$1" == "direct" ]; then
  LOAD_DIRECT=true
  shift
fi
if [ "$1" == "reindex" ]; then
  if [ "$EUID" -ne 0 ]; then
    echo "For reindex, please run as root"
    exit
  fi
  ACTION=reindex
  RECREATE_TEXTINDEX_ONLY=true
  shift
fi

DATASET=$1
shift

if [ "$RECREATE_TEXTINDEX_ONLY" == "true" ]; then
  echo Only recreate text index
else

  GRAPH=$1
  shift

  # remaining parameter(s) for input
  if [ -d $1 ]
  then
    # a directory of files
    FILES="$1/*"
    LOAD_FILES=true
  else
    if [ -f $1 ]; then
      # a single file
      FILES=$@
      LOAD_FILES=true
    else
      echo Load files missing
      exit 1
    fi
  fi

  # optionally, load to named graph
  if [ $GRAPH == "default" ]; then
    GRAPH_OPTION=""
  else
    GRAPH_OPTION=--graph=$GRAPH
  fi
fi

# logfile
LOG=/var/log/fuseki/tdb/${ACTION}_tdb_`date +%Y%m%d`.log
touch $LOG
if [ $? -ne 0 ]; then
  echo Cannot write $LOG
  exit 1
fi

# set directories and optionally cleanup tdb
# (text dir is cleaned up always)
if [ "$RECREATE_TEXTINDEX_ONLY" == true ]; then
  DIR=`readlink -m /var/lib/fuseki/databases/$DATASET`
  TEXT_DIR=`readlink -m /var/lib/fuseki/databases/$DATASET/text`
else
  DIR=`readlink -m /var/lib/fuseki/databases/temp`
  TEXT_DIR=`readlink -m /var/lib/fuseki/databases/temp/text`

  # cleanup tdb
  # xloader insists on creating the directory itself
  if [ "$TDB_LOADER" = "tdb2.xloader" ]; then
    rm -rf $DIR
  else 
    if [ -d $DIR ] && [ -w $DIR ]; then
      rm -rf $DIR/*
    else
      echo "Directory $DIR missing or nor writeable"
      exit 1
    fi
  fi
fi

# put some documentation in the log file
echo `date +"%Y-%m-%d %T"` start run > $LOG
echo "" >> $LOG
##echo "TDB dir: $DIR" >> $LOG
echo "Files (or reindex): $FILES" >> $LOG
echo "" >> $LOG
echo "Configuration:" >> $LOG
java -version >> $LOG 2>&1
echo "Fuseki version:"  >> $LOG
$FUSEKI_HOME/fuseki-server -version >> $LOG 2>&1
echo "Jena version:" >> $LOG
$JENAROOT/bin/jena.version >> $LOG 2>&1
echo "JAVA_OPTS: $JAVA_OPTS" >> $LOG
echo "Loader: $TDB_LOADER" >> $LOG
echo "LOADER_OPTS: $LOADER_OPTS" >> $LOG

if [ "$LOAD_FILES" == "true" ]; then

  # load all files in one step
  echo $JENAROOT/bin/$TDB_LOADER $LOADER_OPTS --loc=$DIR $GRAPH_OPTION $FILES
  $JENAROOT/bin/$TDB_LOADER $LOADER_OPTS --loc=$DIR $GRAPH_OPTION $FILES 2>&1 | tee -a $LOG
  if [ $? -eq 0 ]
  then
    echo `date +"%Y-%m-%d %T"` finished loading $FILE >> $LOG
    echo "" >> $LOG
  else
    echo `date +"%Y-%m-%d %T"` ABORTED >> $LOG
    exit 1
  fi

  # show stats and write to file w/o warning
  echo "Database stats after loading $FILES:" >> $LOG
  java -cp $FUSEKI_HOME/fuseki-server.jar tdb2.tdbstats --loc=$DIR $GRAPH_OPTION | tee -a $LOG
fi

#################################################
# text index

rm -rf $TEXT_DIR/*
mkdir -p $TEXT_DIR
## rw for current user and fuseki
chmod 777 $TEXT_DIR

echo "" >> $LOG
echo "Create temporary text indexing assembler file" | tee -a $LOG

cat >/tmp/temp.ttl << EOF
@prefix :        <http://localhost/jena_example/#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix tdb2:    <http://jena.apache.org/2016/tdb#>
@prefix ja:      <http://jena.hpl.hp.com/2005/11/Assembler#> .
@prefix text:    <http://jena.apache.org/text#> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix skosxl:  <http://www.w3.org/2008/05/skos-xl#> .
@prefix gndo:    <https://d-nb.info/standards/elementset/gnd#> .
@prefix zbwext:  <http://zbw.eu/namespaces/zbw-extensions/> .

## Initialize TDB2
tdb2:DatasetTDB2  rdfs:subClassOf  ja:RDFDataset .
tdb2:GraphTDB2    rdfs:subClassOf  ja:Model .

## Initialize text query
[] ja:loadClass       "org.apache.jena.query.text.TextQuery" .
# A TextDataset is a regular dataset with a text index.
text:TextDataset      rdfs:subClassOf   ja:RDFDataset .
# Lucene index
text:TextIndexLucene  rdfs:subClassOf   text:TextIndex .

:text_dataset rdf:type     text:TextDataset ;
    text:dataset   <#dataset> ;
    text:index     <#indexLucene> ;
    .

# A TDB datset used for RDF storage
<#dataset> rdf:type      tdb2:DatasetTDB2 ;
    tdb2:location "$DIR" ;
    tdb2:unionDefaultGraph true ; # Optional
    .

# Text index description
<#indexLucene> a text:TextIndexLucene ;
    text:directory <file:$TEXT_DIR> ;
    text:storeValues true;
    text:entityMap <#entMapFull> ;
    .
EOF
# append entity map definitions from the end of the Fuseki config file
csplit --quiet --prefix=/tmp/maps /etc/fuseki/config.ttl  "%## Text index definitions ##%"
cat /tmp/maps00 >> /tmp/temp.ttl
rm /tmp/maps00

echo "----------- Temporary config temp.ttl used by textindexer" >> $LOG
cat /tmp/temp.ttl >> $LOG
echo "---------------------------------------------------------" >> $LOG
echo "" >> $LOG

# actual indexing
echo "Use fuseki $TEXT_INDEXER for building index" >> $LOG
java -cp $FUSEKI_HOME/fuseki-server.jar jena.textindexer --debug --desc=/tmp/temp.ttl 2>&1 | tee -a $LOG
if [ $? -eq 0 ]
then
  echo `date +"%Y-%m-%d %T"` finished full-text indexing $FILE >> $LOG
  echo "" >> $LOG
else
  echo `date +"%Y-%m-%d %T"` ABORTED >> $LOG
  exit 1
fi
rm /tmp/temp.ttl

# recreated index owned by fuseki user
if [ "$RECREATE_TEXTINDEX_ONLY" == true ]; then
  chown -R $USER $TEXT_DIR
  chmod 775 $TEXT_DIR
  chmod 665 $TEXT_DIR/*
fi

# replace files in a second step
if [ "$LOAD_FILES" == true ]; then
  # Create README for dataset, indicating success
  echo "`date +"%Y-%m-%d %T"`: Dataset $DATASET built from $FILES, log at $LOG" > "$DIR/README"

  echo " "
  echo "Additional steps:"
  echo " - Stop service"
  echo " - Move $DATASET from $DIR to the prod locations"
  echo " - Start service"
  echo "by procedure: /opt/thes/bin/replace_tdb.sh $DATASET"
  echo " "

  if [[ "e6810f891672" =~ `hostname -s` ]]; then
    echo "On hal-fuseki, copy files to ite-srv26, before executing replace_tdb.sh"
    echo "  rsync -ravu /var/lib/fuseki/databases/temp 134.245.93.73:/var/lib/fuseki/databases/"
    echo " "
  fi
fi

echo "" >> $LOG
echo `date +"%Y-%m-%d %T"` end run >> $LOG


