#!/bin/sh
# jneubert, 2024-04-10

# open a tunnel connection to the IFIS database host in background
ssh -f -N -L 5433:2os3ceki6ym.svc.trove.eqiad1.wikimedia.cloud:5432 bastion.wmcloud.org

