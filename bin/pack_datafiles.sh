#!/bin/sh
# jneubert, 2024-06-04

# pack all pm20 datafiles (including endpoint dump and reports) for transfer to
# ZBW (via Zenodo)

today=`date "+%F"`
packed_dir='../packed'

cd ../data/rdf

archive1=pm20_data_${today}.tgz
echo Pack $archive1 ...
tar czf $packed_dir/$archive1 pm20.extended.jsonld film.jsonld geo.skos.extended.jsonld subject.skos.extended.jsonld ware.skos.extended.jsonld film.jsonld

archive2=endpoint_${today}.tgz
echo Pack $archive2 ...
tar czf $packed_dir/$archive2 pm20_endpoint_all.nq

cd ../sparql-results

archive3=reports_${today}.tgz
echo Pack $archive3 ...
tar czf $packed_dir/$archive3 *.json

echo Manually upload these 3 files to https://doi.org/10.5281/zenodo.11472914 as new version

