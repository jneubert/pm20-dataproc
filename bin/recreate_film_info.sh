#!/bin/sh
# nbt, 2020-02-17

# read information from filmlists and zotero and merge to film.ttl

set -e


BASE_DIR=/opt/pm20-dataproc
LOG_DIR=$BASE_DIR/data/log
echo "`date "+%F %T"`   start recreate film info"

cd $BASE_DIR/bin

# replace relevant tables of local replica
# TODO deactivated - replacement of selected tables does not work properly
# for now, use complete replacement by nightly rsnap_ifis

# create tunnel
###ssh -f -N -L 5433:2os3ceki6ym.svc.trove.eqiad1.wikimedia.cloud:5432 bastion.wmcloud.org

# dump selected tables (should list all tables modified by editors)
###for table in institution klassifikator ; do
###  pg_dump -h localhost -p 5433 -U root -t $table ifis | psql -h localhost -U root ifis
###  echo Reloaded $table
###done

# close tunnel
###kill -9 $(lsof -i :5433 | tail -n 1 | awk -F' ' '{print $2}')


# LOOKUP FUNCTIONS ARE OBTAINED LOCALLY (via modules, based on .jsonld files)

# expand filmlist data with labels and ids
perl expand_signatures.pl
echo "`date "+%F %T"`   signatures expanded"

# mount pm20-filmlink directory (unmount before, if still mounted ...)
REMOTE_DIR=/mnt/remote
if mountpoint -q "$REMOTE_DIR" ; then
  umount "$REMOTE_DIR"
fi
sshfs -v -o allow_other -o workaround=rename jneubert@login.toolforge.org:/data/project/pm20-filmlink $REMOTE_DIR

# filmlist are not any more read on ite-srv24 from jira and copied over,
# so it is not necessary to recreate the html here
## perl create_filmlists.pl > $LOG_DIR/create_filmlists.log 2>&1

for subset in h1_sh h1_co h1_wa h2_co h2_sh h2_wa ; do
  log_file=$LOG_DIR/read_zotero.$subset.log
  perl read_zotero.pl $subset  > $log_file 2>&1
  chmod g+w $log_file
  cp --preserve=timestamps,mode $log_file $REMOTE_DIR/public_html/tmp/filmdata
echo "`date "+%F %T"`     $subset read from zotero"
done
echo "`date "+%F %T"`   zotero data extracted"

umount $REMOTE_DIR

# produce rdf
perl merge_film_ids.pl
echo "`date "+%F %T"`   data merged to film.ttl"

## TODO filmviewer links must be created on pm20
###perl create_filmviewer_links.pl $subset > $LOG_DIR/create_filmviewer_links.$subset.log 2>&1

