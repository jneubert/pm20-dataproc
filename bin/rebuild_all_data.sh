#!/bin/sh
# nbt, 28.2.2018

# recreate the PM20 data files locally, in order to transfer them to the
# pm20.zbw.eu server
#
# Preconditions:
# - image and docdata files cannot be recreated without access to the
#   pm20.zbw.eu file system, they are considered as static here
# - folder and vocab data is extracted from a local copy of the IFIS database,
#   which has to be updated beforehand
# - the fuseki pm20 database has to be wiped beforehand (requires sudo)
# - Perl libraries are located via PERL5LIB (to be supplied for cron job)
#
# Process:
# 1. read folder and vocab data from ifis db, save as rdf
# 2. load into local pm20 endpoint and expand the data via (federated/Wikidata)
#    queries
# 3. dump extended versions of folder and vocab data as .extended.ttl
# 4. transform to .jsonld
# 5. read film data from filmlists and zotero, expand them via Folder/Vocab.pm
#    (based on the .jsonld files), save as film.ttl, load into local endpoint
#    and transform to film.jsonld
#
# 6. TODO create minimal package for data transfer

# stop at error
set -e

cd /opt/pm20-dataproc/bin
echo "`date "+%F %T"` start rebuild all data"

# create rdf output
/usr/bin/perl create_rdf1.pl > ../data/rdf/pm20.ttl 2> /dev/null
echo "`date "+%F %T"` done create pm20 rdf"

# create skos vocabularies
# (and vocabulary map files for apache)
# (rebuild_exception_lists.sh already runs hourly)
/usr/bin/perl create_skos.pl
echo "`date "+%F %T"` done create skos"

# copy vocabulary map files for apache to pm20 server
# TODO currently not handled
##scp -pq ../data/klassdata/ag_map.txt nbt@pm20:/disc1/pm20/data/url_map/geo_sig2id.txt
##scp -pq ../data/klassdata/je_map.txt nbt@pm20:/disc1/pm20/data/url_map/subject_sig2id.txt
##scp -pq ../data/klassdata/ip_map.txt nbt@pm20:/disc1/pm20/data/url_map/ware_sig2id.txt
####echo "`date "+%F %T"` done copy map files"

# recreate sparql endpoint (and extend data)
./recreate_pm20_dev_endpoint.sh
echo "`date "+%F %T"` done recreate endpoint"

# dump all RDF from endpoint (with all extensions)
# (must be executed on remote machine, to have endpoint directly accessible)
curl --silent -X GET -H "Accept: text/turtle" http://localhost:3030/pm20/get?graph=default > ../data/rdf/pm20.extended.ttl
for vocab in geo subject ware ; do
  vocab_graph=http://zbw.eu/beta/$vocab/ng
  curl --silent -X GET -H \"Accept: text/turtle\" http://localhost:3030/pm20/get?graph=$vocab_graph > ../data/rdf/${vocab}.skos.extended.ttl
done
echo "`date "+%F %T"` done dump rdf"

# convert to jsonld

# GET as jsonld is much faster than transformation with riot
##JVM_ARGS="-Xms4g" /opt/jena/bin/riot --output=jsonld ../data/rdf/pm20.extended.ttl > ../data/rdf/pm20.interim.jsonld
curl --silent -X GET -H "Accept: application/ld+json" http://localhost:3030/pm20/get?graph=default > ../data/rdf/pm20.interim.jsonld
echo "`date "+%F %T"` done get pm20.interim.jsonld"

# TODO better done with ld-cli? (is frame necessary?)
# transformation via PyLD (instead of php jsonld) extends subordinated nodes
python3 transform_jsonld.py frame_folder ../data/rdf/pm20.interim.jsonld > ../data/rdf/pm20.extended.jsonld
echo "`date "+%F %T"` transform_jsonld.py done "

# transform vocabularies
for vocab in geo subject ware ; do
  input=../data/rdf/${vocab}.skos.extended.ttl
  interim=../data/rdf/${vocab}.skos.interim.jsonld
  output=../data/rdf/${vocab}.skos.extended.jsonld

  # convert without context applied
  /opt/jena/bin/riot --output=jsonld $input > $interim
  # flatten and compact using context from /pm20,
  # via https://github.com/filip26/ld-cli
  /usr/local/bin/ld-cli flatten -c http://localhost/schema/context.jsonld -p  < $interim > $output
done
echo "`date "+%F %T"` done skos.extended.ttl to .jsonld"

# film data

# read and expand film information (based on updated vocabs)
./recreate_film_info.sh
echo "`date "+%F %T"` done recreate_film_info"

# load film graph
curl --silent --show-error -X POST -H "Content-type: text/turtle" \
  --data-binary @../data/rdf/film.ttl http://localhost:3030/pm20/data?graph=http://zbw.eu/beta/film/ng > /dev/null
echo "`date "+%F %T"` done load film graph"

# transform film data
input=../data/rdf/film.ttl
interim=../data/rdf/film.interim.jsonld
output=../data/rdf/film.jsonld
/opt/jena/bin/riot --output=jsonld $input > $interim
/usr/local/bin/ld-cli flatten -c http://localhost/schema/context.jsonld -p < $interim > $output
echo "`date "+%F %T"` done film.ttl to .jsonld"


# create sparql results
# (both scripts based on sparql_results.yaml)
/usr/bin/perl mk_sparql_results.pl
echo "`date "+%F %T"` done make sparql results"

# dump endpoint (full and sorted non-blank-node file)
./dump_pm20_dev_endpoint.sh
echo "`date "+%F %T"` done dump endpoint"

echo "`date "+%F %T"` end"


