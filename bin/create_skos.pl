#!/bin/perl
# nbt, 31.1.2018

# create skos files for ifis klassifikator table

use strict;
use warnings;
use lib '../lib';
use utf8;

use Carp;
use Data::Dumper;
use DateTime;
use JSON;
use Path::Tiny;
use ZBW::Ifis::Schema;

#$Data::Dumper::Sortkeys = 1;
binmode( STDOUT, ":utf8" );

my @holdings        = qw/ kl /;
my $beta_root_uri   = 'http://zbw.eu/beta/pm20voc/';
my $root_uri        = 'https://pm20.zbw.eu/category/';
my $old_root_uri    = 'http://purl.org/pressemappe20/category/';
my $folderdata_root = path('../data/folderdata');
my $klassdata_root  = path('../data/klassdata');
my $rdf_root        = path('../data/rdf');

my %scheme = (
  ag => { title => 'Historischer Ländercode', category_type => 'geo', },
  gk => { title => 'Ländercode', },
  ip => { title => 'Warensystematik', category_type => 'ware', },
  je => { title => 'Sachsystematik',  category_type => 'subject', },
  na => { title => 'NACE', },
  pr => { title => 'Presseklassifikation (neu)', },
  sk => { title => 'Standardklassifikation (Branche)', },
);

my %text_typ = (
  ##  85 => 'skos:scopeNote',
  86 => 'skos:note',             # Erläuterung (klass_beschr_dt)
  87 => 'skos:scopeNote',        # Ausschluss (klass_ausschl_dt)
  88 => 'skos:editorialNote',    # Interner Hinweis (klass_int_hinw)
  91 => 'skos:scopeNote',        # Einschluss (klass_einschl)
);

my %lang_code = (
  6 => 'de',
  7 => 'en',
);

my %lookup;
my @lookup_vocabs = qw/ ag je ip /;

# Apache rewrite map nta -> id
my %map;
my @map_vocabs = qw/ ag je /;

# for check of duplicate notations
my %nta_count;

our $debug = 0;
my $schema = ZBW::Ifis::Schema->get_schema();

my $main_rs;

# create skos schemes
foreach my $vocab ( sort keys %scheme ) {

  # get latest change for the particular vocabulary
  $main_rs =
    $schema->resultset('Klassifikator')->search( { klass_code => uc($vocab) },
    { order_by => { -desc => 's_timestamp' } } );
  my $modified = $main_rs->first->s_timestamp->ymd;

  my ( $category_type, $scheme_uri, $fh );
  if ( exists $scheme{$vocab}{category_type} ) {
    $category_type = $scheme{$vocab}{category_type};
    $fh            = $rdf_root->child("$category_type.skos.ttl")->openw_utf8;
    $scheme_uri    = "$root_uri$category_type";
  } else {
    $fh         = $rdf_root->child("$vocab.skos.ttl")->openw_utf8;
    $scheme_uri = "$beta_root_uri$vocab";
  }
  $scheme{$vocab}{fh} = $fh;

  # print turtle header
  print $fh "\@prefix skos: <http://www.w3.org/2004/02/skos/core#> .\n"
    . "\@prefix dc: <http://purl.org/dc/elements/1.1/> .\n"
    . "\@prefix dct: <http://purl.org/dc/terms/> .\n"
    . "\@prefix gndo: <https://d-nb.info/standards/elementset/gnd#> .\n"
    . "\@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
    . "\@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n"
    . "\@prefix schema: <http://schema.org/> .\n"
    . "\@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n"
    . "\@prefix owl: <http://www.w3.org/2002/07/owl#> .\n"
    . "\@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n"
    . "\@prefix zbwext: <http://zbw.eu/namespaces/zbw-extensions/> .\n\n";

  print $fh "<$scheme_uri> a skos:ConceptScheme ;\n"
    . "  dct:title \"$scheme{$vocab}{title}\"\@de ;\n"
    . "  dct:published \""
    . DateTime->now->datetime
    . "\"^^xsd:datetime .\n";
  if ($modified) {
    print $fh "<$scheme_uri> dct:modified \"$modified\"^^xsd:date .\n";
  }

  $main_rs = $schema->resultset('Klassifikator')
    ->search( { klass_code => uc($vocab) }, { order_by => 'klass_not' } );

  # main loop
  my %notation;
  while ( my $rec = $main_rs->next ) {

    my $id            = $rec->id;
    my $notation_long = $rec->klass_not;
    my $nta           = $rec->klass_not_short;
    my $fh            = $scheme{$vocab}{fh};

    # URI depends (published category or beta vocab)
    my $uri;
    if ($category_type) {
      $uri = "<$scheme_uri/i/$id>";
    } else {
      $uri = "<$scheme_uri/$id>";
    }

    # skip entries in je
    if ( $vocab eq 'je' ) {

      # skip top level JE entries which are not part of the category system
      next if not $notation_long =~ m/^([a-q]|JE)/;

      # skip artificial levels ending with '-' (e.g., 'n 04-')
      next if $notation_long =~ m/\-$/;

      # skip artificial 'nv'
      next if $notation_long =~ m/^nv$/;

      # skip artificial 00 level for Sondermappen
      next if $notation_long =~ m/ 00$/;
    }

    print $fh "$uri rdf:type skos:Concept .\n";
    print $fh "$uri skos:inScheme <$scheme_uri> .\n";
    print $fh "$uri dct:identifier \"$id\" .\n";
    print $fh "$uri skos:prefLabel ", quote( $rec->name_term ), "\@de .\n";
    if ( $rec->name_term_en ) {
      print $fh "$uri skos:prefLabel ", quote( $rec->name_term_en ), "\@en .\n";
    }

    # for published categories with persistent URIs
    if ($category_type) {

      # add old purl URIs
      if ( $vocab eq 'je' or $vocab eq 'ag' or $vocab eq 'ip' ) {
        print $fh "$uri owl:sameAs <${old_root_uri}$category_type/i/"
          . "$id> . \n";
      }

      # add extensible "notation" URLs used from Wikidata
      if ( $vocab eq 'je' or $vocab eq 'ag' ) {
        print $fh "$uri owl:sameAs <$scheme_uri/s/"
          . encode_nta($nta)
          . "> . \n";
      }

      # add link to actual documentation page with folder/image links
      if ( $vocab eq 'je' or $vocab eq 'ag' or $vocab eq 'ip' ) {
        print $fh "$uri schema:subjectOf <${root_uri}$category_type/i/"
          . "$id/about> . \n";
      }
    }

    # export both long and short form of notation
    print $fh "$uri zbwext:notationLong \"$notation_long\"^^xsd:string .\n";
    print $fh "$uri skos:notation \"$nta\"^^xsd:string .\n";

    # additional fields
    if ( defined $rec->gnd_id ) {
      print $fh "$uri gndo:gndIdentifier \"", $rec->gnd_id, "\" .\n";
    }

    if ( defined $rec->komplett ) {
      print $fh "$uri zbwext:foldersComplete \"", $rec->komplett, "\" .\n";
    }

    if ( $vocab eq 'ag' and defined $rec->typ ) {
      print $fh "$uri zbwext:geoCategoryType \"", $rec->typ, "\" .\n";
    }

    # zbwext:folderCount is added later according to actual data in the file
    # system. Categories without existing folders, which are needed for
    # structural reasons (levels in hiearchy) are marked with "mappen_anzahl =
    # 0" in the database, and the folderCount has to be set here
    if (  $vocab eq 'je'
      and defined $rec->mappen_anzahl
      and $rec->mappen_anzahl == 0 )
    {
      print $fh "$uri zbwext:folderCount 0 .\n";
    }

    # lookup file
    $lookup{$vocab}{$nta} = $rec->name_term;
    $map{$vocab}{$nta}    = $id;

    # broader/narrower terms
    my $broader_rs =
        $vocab eq 'je'
      ? $rec->klass_broader_je_relations
      : $rec->klass_broader_relations;
    if ( my $broader = $broader_rs->next ) {
      print $fh "$uri skos:broader <$scheme_uri/i/"
        . $broader->nr_klass . "> .\n";
    }
    my $narrower_rs =
        $vocab eq 'je'
      ? $rec->klass_narrower_je_relations
      : $rec->klass_narrower_relations;
    while ( my $narrower = $narrower_rs->next ) {
      print $fh "$uri skos:narrower <$scheme_uri/i/"
        . $narrower->nr_klass . "> .\n";
    }

    # texts
    my $scope_note = '';
    my $text_rs    = $rec->klass_text_relations;
    while ( my $text_rec = $text_rs->next ) {

      next unless ( $text_rec->text );

      my $text = $text_rec->text;
      $text =~ s/\s+$//;

      # GND (in the first part of the field)
      if ( $text_rec->nr_texttyp == 100 ) {
        my ($gnd) = split( /;/, $text );
        print $fh "$uri skos:exactMatch <https://d-nb.info/gnd/$gnd> .\n";
        print $fh "$uri gndo:gndIdentifier \"$gnd\" .\n";

        # Geonames
      } elsif ( $text_rec->nr_texttyp == 120 ) {
        if ( $text =~ m/^[0-9X]+$/ ) {
          print $fh "$uri skos:exactMatch <http://geonames.org/$text> .\n";
        }
        print $fh "$uri zbwext:geoIdentifier \"" . $text . "\" .\n";
      } elsif ( $text_rec->nr_texttyp == 86 ) {
        $scope_note .= $text;
      } elsif ( $text_rec->nr_texttyp == 91 ) {
        $scope_note .= ' (einschl.: ' . $text . ')';
      } elsif ( $text_rec->nr_texttyp == 87 ) {
        $scope_note .= ' (ausschl.: ' . $text . ')';
      } else {
        if ( not $text_typ{ $text_rec->nr_texttyp } ) {
          warn "Unknown texttyp " . $text_rec->nr_texttyp . "\n";
          next;
        }
        my $skos_rel = $text_typ{ $text_rec->nr_texttyp };
        print $fh "$uri $skos_rel " . quote($text) . "\@de .\n";
      }
    }

    # only German entries (outside TW)
    if ( $scope_note ne '' ) {

      # newlines should not occur and must be replaced (in order to aviod
      # breaking markdown reports)
      $scope_note =~ s/\n/ /g;
      $scope_note =~ s/^ (.*)/$1/;
      print $fh "$uri skos:scopeNote " . quote($scope_note) . "\@de .\n";
    }
  }
}

# write lookup files
foreach my $vocab (@lookup_vocabs) {
  $klassdata_root->child("${vocab}_lookup.json")
    ->spew( encode_json( $lookup{$vocab} ) );
}

# write map files (for apache rewrites)
foreach my $vocab (@map_vocabs) {
  my $data;
  foreach my $nta ( keys %{ $map{$vocab} } ) {
    $data .= encode_nta($nta) . " $map{$vocab}{$nta}\n";
  }
  $klassdata_root->child("${vocab}_map.txt")->spew($data);
}

# duplicates check
foreach my $voc ( keys %nta_count ) {
  foreach my $nta ( keys %{ $nta_count{$voc} } ) {
    next if ( $nta_count{$voc}{$nta} == 1 );
    warn "Warning: $nta_count{$voc}{$nta} occurrences of $nta\n";
  }
}

#####################

sub load_dat {
  my $holding = shift or confess("param missing");

  ##my $dat_file = $folderdata_root->child("test_${holding}_dat.json");
  my $dat_file = $folderdata_root->child("${holding}_dat.json");
  my $dat_ref  = decode_json( $dat_file->slurp );

  my %dat;
  foreach my $rec_ref ( @{$dat_ref} ) {
    my @ids;
    my $id = sprintf( "%06d", $rec_ref->{NOT00} );
    push( @ids, $id );
    my $rec_id = $holding . '/' . join( ',', @ids );
    $dat{$rec_id} = $rec_ref;
  }
  return \%dat;
}

sub quote {
  my $text = shift or confess("param missing");

  # remove trailing space
  $text =~ s/\s+$//;

  # save quoting for turtle
  my $q = '"""';

  # error with with trailing single quote, so:
  if ( $text =~ m/"$/ ) {
    $q = "'''";
  }

  return $q . $text . $q;
}

sub encode_nta {
  my $nta = shift or confess("param missing");

  # TODO for now, replace whitespace with "_"
  ( my $nta_enc = $nta ) =~ s/ /_/g;
  return $nta_enc;
}
