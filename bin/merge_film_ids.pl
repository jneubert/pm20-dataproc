#!/bin/perl
# nbt, 2024-01-15

# merge info from zotero and filmlists by id and print it as turtle

use strict;
use warnings;
use utf8;

use Carp;
use Data::Dumper;
use JSON;
use Path::Tiny;
use Readonly;
use ZBW::PM20x::Film;

binmode( STDOUT, ":utf8" );
binmode( STDERR, ":utf8" );
$Data::Dumper::Sortkeys = 1;

Readonly my $FILMDATA_ROOT => path('../data/filmdata');
Readonly my $ROOT_URI      => 'https://pm20.zbw.eu/';
Readonly my $OUT_FILE      => path('../data/rdf/film.ttl');

# for output of rdf data
my @output_lines = prefixes();

foreach my $filming (qw/ 1 2 /) {

  # we only deal with HWWA (h) films films here
  my $set     = "h${filming}";
  my $set_uri = $ROOT_URI . "film/$set";
  pt( $set_uri, 'a', 'zbwext:Pm20FilmSet' );

  foreach my $collection (qw/ co sh wa /) {

    my %grp_prop = %{ ZBW::PM20x::Film->get_grouping_properties($collection) };

    my $subset     = "${set}_$collection";
    my $subset_uri = "${set_uri}/$collection";
    pt( $subset_uri, 'a',            "zbwext:Pm20FilmSubset" );
    pt( $subset_uri, 'dct:isPartOf', "$set_uri" );

    # file names
    my $zotero_name   = "zotero.${subset}.json";
    my $filmlist_name = "${subset}.expanded.json";

    # target data structure
    my %by_id;

    # films the filmlist file, reorganized as hash keyed by film name
    my $templist_ref =
      decode_json( $FILMDATA_ROOT->child($filmlist_name)->slurp );
    my $filmlist_ref;
    foreach my $entry_ref ( @{$templist_ref} ) {
      $filmlist_ref->{ $entry_ref->{film_id} } = $entry_ref;
    }

    # film sections from zotero
    my $zotero_ref = decode_json( $FILMDATA_ROOT->child($zotero_name)->slurp );

    # iterate of over all (non-empty) films of the subset
    my $primary_group_id;
    my $number_of_item_images = 0;
    my @films                 = ZBW::PM20x::Film->films($subset);
    ##print Dumper \@films;exit;
    foreach my $film (@films) {

      my $film_name         = $film->name;
      my $filmlistentry_ref = $filmlist_ref->{$film_name};

      # skip film if it is already online
      next unless $filmlistentry_ref->{online} eq '';

      ##print "$film_name\n";

      # trucate _1 ids
      my $zotero_film_id = $film->logical_name();

      # for the RDF structure, use physical film id
      my $film_uri = "$subset_uri/$film_name";
      pt( $film_uri, 'a',                      "zbwext:Pm20Film" );
      pt( $film_uri, 'dct:isPartOf',           "$subset_uri" );
      pt( $film_uri, 'zbwext:totalImageCount', $film->img_count() );

      # when information from zotero exists, use preferably that
      if ( $zotero_ref->{$zotero_film_id} ) {
        ###print "$film_name  \tfrom zotero\n";
        # skip _2 films because in zotero _1 and _2 do not exist -
        # data has already been read at that point
        next if $film_name =~ m/_2$/;

        # all sections defined for this film in zotero
        my @items = sort keys %{ $zotero_ref->{$zotero_film_id}{item} };

        # add the entry of a continuation, if it is missing in zotero
        if ( not $items[0] =~ m;$zotero_film_id(?:_[12])?/000[123](/[RL])?$; ) {

          # compute the number of images up to the first zotero entry as the
          # number of images for this item
          $items[0] =~ m;$zotero_film_id(?:_[12])?/(\d{4})(/[RL])?$;;
          $number_of_item_images = $1 - 2;

          ##print Dumper $zotero_film_id, $items[0], $filmlistentry_ref
          ##  if $number_of_item_images == 0;
          $primary_group_id =
            add_entry_from_filmlist( $film_uri, $filmlistentry_ref,
            $number_of_item_images, \%grp_prop );

        }

        foreach my $location (@items) {
          my %data = %{ $zotero_ref->{$zotero_film_id}{item}{$location} };

          my %prim_grp_prop = %{ $grp_prop{primary_group} };
          $primary_group_id = $data{ $prim_grp_prop{zotero} } || 0;

          if ( not $data{title} ) {
            print "missing title: ", Dumper $location, \%data;
            next;
          }

          # extract the starting position part from $location
          my $start_pos;
          my @location_elements = split( '/', $location );
          if ( scalar @location_elements == 6 ) {
            $start_pos = "$location_elements[4]/$location_elements[5]";
          } elsif ( scalar @location_elements == 5 ) {
            $start_pos = $location_elements[4];
          } else {
            warn "strange location $location\n";
          }

          my $item_uri = "$film_uri/$start_pos";
          pt( $item_uri, 'a',            'zbwext:Pm20FilmItem' );
          pt( $item_uri, 'dct:isPartOf', $film_uri );
          if ( $data{signature_string} and $data{signature_string} ne '' ) {
            pt( $item_uri, 'skos:notation', quote( $data{signature_string} ) );
          }
          pt( $item_uri, 'dct:source', quote('zotero') );

          # title for the marker (not validated, not guaranteed
          # to cover the whole stretch of images up to the next marker
          ## TODO improve: for English, derive from IDs
          pt( $item_uri, 'dct:title', quote( $data{title} ) );

          if ( $data{qid} ) {
            pt( $item_uri, 'zbwext:wdIdentifier', quote( $data{qid} ) );
          }

          if ( $data{start_date} ) {
            pt( $item_uri, 'schema:startDate', quote( $data{start_date} ) );
          }

          # if present, add identifier for primary group
          if ( $primary_group_id and $primary_group_id ne '-' ) {

            ## remove "co/" from id (in case of collection co)
            $primary_group_id =~ s/^co\/(\d{6})$/$1/;

            pt(
              $item_uri,
              $prim_grp_prop{rdf_pred},
              "$prim_grp_prop{rdf_prefix}:$primary_group_id"
            );
          }

          # compute totals
          if ( exists $data{number_of_images} ) {
            pt( $item_uri, 'zbwext:totalImageCount', $data{number_of_images} );
            $by_id{$primary_group_id}{total_number_of_images} +=
              $data{number_of_images};
          } else {
            warn "number_of_images missing for $location\n";
          }
        }    # $location
      }
      #
      # when there is no information from zotero, add the film "in toto" from
      # the filmlist entry
      else {
        ###print "$film_name  \tfrom filmlist\n";

        # number of images in this item
        if ( $film->img_count() ) {
          $number_of_item_images = $film->img_count();
        } else {
          warn "number of images for full film $film_name not found\n";
          $number_of_item_images = 0;
        }

        $primary_group_id =
          add_entry_from_filmlist( $film_uri, $filmlistentry_ref,
          $number_of_item_images, \%grp_prop );

        # skip items with un-identified primary group
        next unless $primary_group_id;

        # update total_number_of_images
        $by_id{$primary_group_id}{total_number_of_images} +=
          $number_of_item_images;
      }
    }
  }
}

$OUT_FILE->spew_utf8( join( "\n", @output_lines ) );

####

# print triple as turtle
sub pt {
  my $s = shift or confess "param missing";
  my $p = shift or confess "param missing";
  my $o = shift or confess "param missing";

  if ( $s =~ m;^http(s)?://; ) {
    $s = "<$s>";
  }

  # identify and format uris
  if ( $o =~ m;^http(s)?://; ) {
    $o = "<$o>";
  }

  push( @output_lines, "$s $p $o ." );
}

sub prefixes {
  my $prefixes = <<'EOF';
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix fi: <https://pm20.zbw.eu/film/> .
@prefix gndo: <https://d-nb.info/standards/elementset/gnd#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix pm20co: <https://pm20.zbw.eu/folder/co/> .
@prefix pm20geo: <https://pm20.zbw.eu/category/geo/i/> .
@prefix pm20subject: <https://pm20.zbw.eu/category/subject/i/> .
@prefix pm20ware: <https://pm20.zbw.eu/category/ware/i/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix zbwext: <http://zbw.eu/namespaces/zbw-extensions/> .

EOF

  return split( "\n", $prefixes );
}

sub add_entry_from_filmlist {
  my $film_uri              = shift or confess "param missing";
  my $filmlistentry_ref     = shift or confess "param missing";
  my $number_of_item_images = shift or confess "param missing";
  my $grp_prop_ref          = shift or confess "param missing";

  my $item_uri = "$film_uri/0001";
  pt( $item_uri, 'a',            'zbwext:Pm20FilmItem' );
  pt( $item_uri, 'dct:isPartOf', $film_uri );
  if ( my $signature = $filmlistentry_ref->{start_sig_plain} ) {
    pt( $item_uri, 'skos:notation', quote($signature) );
  }
  pt( $item_uri, 'dct:source', quote('filmlist') );
  pt( $item_uri, 'dct:title',  quote( $filmlistentry_ref->{start_title} ) );
  pt( $item_uri, 'zbwext:totalImageCount', $number_of_item_images );

  my %prim_grp_prop = %{ $grp_prop_ref->{primary_group} };
  my $primary_group_id;
  if ( $primary_group_id = $filmlistentry_ref->{ $prim_grp_prop{filmlist} } ) {
    ## remove "co/" from id (in case of collection co)
    $primary_group_id =~ s/^co\/(\d{6})$/$1/;
    pt(
      $item_uri,
      $prim_grp_prop{rdf_pred},
      "$prim_grp_prop{rdf_prefix}:$primary_group_id"
    );
  }
  return $primary_group_id;
}

sub quote {
  my $text = shift or confess "param missing\n";

  # remove trailing space
  $text =~ s/\s+$//;

  # save quoting for turtle
  my $q = '"""';

  # error with with trailing single quote, so:
  if ( $text =~ m/"$/ ) {
    $q = "'''";
  }

  return $q . $text . $q;
}

