#!/bin/sh
# jneubert, 2024-04-10

# close a tunnel connection to the IFIS database host in background
kill -9 $(lsof -i :5433 | tail -n 1 | awk -F' ' '{print $2}')

