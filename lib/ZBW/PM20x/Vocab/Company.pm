# jneubert, 2024-04-24

package ZBW::PM20x::Vocab::Company;

use strict;
use warnings;
use utf8;

use Carp qw/ cluck confess croak /;
use Data::Dumper;
use Exporter;
use JSON;
use Path::Tiny;
use Readonly;

# exported package constants
our @ISA    = qw/ Exporter /;
our @EXPORT = qw/ @LANGUAGES $SM_QR $DEEP_SM_QR /;

Readonly my $RDF_ROOT => path('../data/rdf');

our %signature_lookup;
our %label;

=encoding utf8

=head1 NAME

ZBW::PM20x::Vocab::Company - Supplement for looking up companies by signature


=head1 SYNOPSIS

  use ZBW::PM20x::Vocab::Company;
  my $voc = ZBW::PM20x::Vocab::Company->new();

  my $company_id = $voc->signature($id);
  my $company_name = $voc->label($company_id);

=head1 Class methods

=over 2

=item new ()

Return a new vocab object from the "company" vocabulary. Read the PM20
vocabluary in JSONLD format into the object.

=cut

sub new {
  my $class = shift or croak('param missing');

  my $self = {};
  bless $self, $class;

  # initialize with file
  my $file = path("$RDF_ROOT/pm20.extended.jsonld");
  my @companies =
    @{ decode_json( $file->slurp )->{'@graph'} };

  # read jsonld graph
  foreach my $company (@companies) {

    my @types = @{ $company->{'@type'} };
    next unless scalar(@types);

    foreach my $type (@types) {
      if ( $type eq 'CompanyFolder' ) {

        my $id = $company->{identifier};

        next unless $company->{notation};

        # add signature to lookup table
        $signature_lookup{ $company->{notation} } = $id;

        # add label
        $label{$id} = $company->{prefLabel};
      }
    }
  }

  return $self;
}

=back

=head1 Instance methods

=over 2


=item label ( $company_id )

Returns company name.

=cut

sub label {
  my $self = shift or croak('param missing');
  my $id   = shift or croak('param missing');

  my $label = $label{$id};

  return $label;
}

=item lookup_signature ( $signature )

Look up a company id by signature. Returns company (folder) id,  undef if not defined.

=cut

sub lookup_signature {
  my $self      = shift or croak('param missing');
  my $signature = shift or croak('param missing');

  my $id = $signature_lookup{$signature};

  return $id;
}

=back

=cut

1;

