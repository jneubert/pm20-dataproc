use utf8;
package ZBW::Ifis::Schema::Result::PubPubart;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PubPubart

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pub_pubart>

=cut

__PACKAGE__->table("pub_pubart");

=head1 ACCESSORS

=head2 nr_pub_pubart

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'pub_pubart_seq'

=head2 nr_publikation

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_pubart

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_pub_pubart",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "pub_pubart_seq",
  },
  "nr_publikation",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_pubart",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_pub_pubart>

=back

=cut

__PACKAGE__->set_primary_key("nr_pub_pubart");

=head1 RELATIONS

=head2 nr_pubart_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Pubart>

=cut

__PACKAGE__->belongs_to(
  "nr_pubart_relation",
  "ZBW::Ifis::Schema::Result::Pubart",
  { nr_pubart => "nr_pubart" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_publikation_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publikation>

=cut

__PACKAGE__->belongs_to(
  "nr_publikation_relation",
  "ZBW::Ifis::Schema::Result::Publikation",
  { nr_publikation => "nr_publikation" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:umrMgsPvSGuqWlSOHCHcDQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
