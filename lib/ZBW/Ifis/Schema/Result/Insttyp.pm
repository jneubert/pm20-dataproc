use utf8;
package ZBW::Ifis::Schema::Result::Insttyp;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Insttyp

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<insttyp>

=cut

__PACKAGE__->table("insttyp");

=head1 ACCESSORS

=head2 nr_insttyp

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'insttyp_seq'

=head2 insttyp_key

  data_type: 'char'
  is_nullable: 0
  size: 4

=head2 insttyp_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 insttyp_en

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_insttyp",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "insttyp_seq",
  },
  "insttyp_key",
  { data_type => "char", is_nullable => 0, size => 4 },
  "insttyp_dt",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "insttyp_en",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_insttyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_insttyp");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_insttyp_dt>

=over 4

=item * L</insttyp_dt>

=back

=cut

__PACKAGE__->add_unique_constraint("u_insttyp_dt", ["insttyp_dt"]);

=head2 C<u_insttyp_key>

=over 4

=item * L</insttyp_key>

=back

=cut

__PACKAGE__->add_unique_constraint("u_insttyp_key", ["insttyp_key"]);

=head1 RELATIONS

=head2 inst_typ_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstTyp>

=cut

__PACKAGE__->has_many(
  "inst_typ_relations",
  "ZBW::Ifis::Schema::Result::InstTyp",
  { "foreign.nr_insttyp" => "self.nr_insttyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iiRFDDKSNOev9PiTn5Q1pA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
