use utf8;
package ZBW::Ifis::Schema::Result::Funkpers;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Funkpers

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<funkpers>

=cut

__PACKAGE__->table("funkpers");

=head1 ACCESSORS

=head2 nr_funkpers

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'funkpers_seq'

=head2 funkpers_key

  data_type: 'char'
  is_nullable: 0
  size: 4

=head2 funkpers_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 funkpers_en

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 funkpers_kz

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_loesch

  data_type: 'char'
  is_nullable: 1
  size: 1

=cut

__PACKAGE__->add_columns(
  "nr_funkpers",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "funkpers_seq",
  },
  "funkpers_key",
  { data_type => "char", is_nullable => 0, size => 4 },
  "funkpers_dt",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "funkpers_en",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "funkpers_kz",
  { data_type => "char", is_nullable => 1, size => 2 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_loesch",
  { data_type => "char", is_nullable => 1, size => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_funkpers>

=back

=cut

__PACKAGE__->set_primary_key("nr_funkpers");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_funkpers_kz_key>

=over 4

=item * L</funkpers_kz>

=item * L</funkpers_key>

=back

=cut

__PACKAGE__->add_unique_constraint("u_funkpers_kz_key", ["funkpers_kz", "funkpers_key"]);

=head2 C<u_funkpers_kz_key_dt>

=over 4

=item * L</funkpers_kz>

=item * L</funkpers_key>

=item * L</funkpers_dt>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "u_funkpers_kz_key_dt",
  ["funkpers_kz", "funkpers_key", "funkpers_dt"],
);

=head1 RELATIONS

=head2 inst_funkpers_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstFunkpers>

=cut

__PACKAGE__->has_many(
  "inst_funkpers_relations",
  "ZBW::Ifis::Schema::Result::InstFunkpers",
  { "foreign.nr_funkpers" => "self.nr_funkpers" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:XQgkkITYu0gvJXmBPHgmsA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
