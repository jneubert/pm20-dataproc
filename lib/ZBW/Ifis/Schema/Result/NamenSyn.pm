use utf8;
package ZBW::Ifis::Schema::Result::NamenSyn;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::NamenSyn

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<namen_syn>

=cut

__PACKAGE__->table("namen_syn");

=head1 ACCESSORS

=head2 nr_name

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 name_syn

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_name",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "name_syn",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_name>

=item * L</name_syn>

=back

=cut

__PACKAGE__->set_primary_key("nr_name", "name_syn");

=head1 RELATIONS

=head2 nr_name_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Namen>

=cut

__PACKAGE__->belongs_to(
  "nr_name_relation",
  "ZBW::Ifis::Schema::Result::Namen",
  { nr_name => "nr_name" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:95UmUcJY75pAofAP7OuC7w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
