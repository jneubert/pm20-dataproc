use utf8;
package ZBW::Ifis::Schema::Result::InstText;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::InstText

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<inst_text>

=cut

__PACKAGE__->table("inst_text");

=head1 ACCESSORS

=head2 nr_inst

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_texttyp

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_sprache

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 0

=head2 inst_text

  data_type: 'varchar'
  is_nullable: 1
  size: 2000

=head2 text

  data_type: 'text'
  is_nullable: 1

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 nr_inst_text

  data_type: 'numeric'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'inst_text_seq'
  size: [38,0]

=cut

__PACKAGE__->add_columns(
  "nr_inst",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_texttyp",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_sprache",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 0 },
  "inst_text",
  { data_type => "varchar", is_nullable => 1, size => 2000 },
  "text",
  { data_type => "text", is_nullable => 1 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "nr_inst_text",
  {
    data_type => "numeric",
    is_auto_increment => 1,
    is_nullable => 0,
    sequence => "inst_text_seq",
    size => [38, 0],
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_inst_text>

=back

=cut

__PACKAGE__->set_primary_key("nr_inst_text");

=head1 RELATIONS

=head2 nr_inst_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->belongs_to(
  "nr_inst_relation",
  "ZBW::Ifis::Schema::Result::Institution",
  { nr_inst => "nr_inst" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 nr_sprache_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Sprache>

=cut

__PACKAGE__->belongs_to(
  "nr_sprache_relation",
  "ZBW::Ifis::Schema::Result::Sprache",
  { nr_sprache => "nr_sprache" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_texttyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Texttypen>

=cut

__PACKAGE__->belongs_to(
  "nr_texttyp_relation",
  "ZBW::Ifis::Schema::Result::Texttypen",
  { nr_texttyp => "nr_texttyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2nYL9pC3DPo8cZj4yt1coA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
