use utf8;
package ZBW::Ifis::Schema::Result::KlassRelJe;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::KlassRelJe

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klass_rel_je>

=cut

__PACKAGE__->table("klass_rel_je");

=head1 ACCESSORS

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 sup_nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "sup_nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_klass>

=item * L</sup_nr_klass>

=back

=cut

__PACKAGE__->set_primary_key("nr_klass", "sup_nr_klass");

=head1 RELATIONS

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 sup_nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "sup_nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "sup_nr_klass" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-15 07:48:32
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cdUyD8dzvfOghJuyyLUoAA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
