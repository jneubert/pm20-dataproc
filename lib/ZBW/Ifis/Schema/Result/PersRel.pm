use utf8;
package ZBW::Ifis::Schema::Result::PersRel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PersRel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pers_rel>

=cut

__PACKAGE__->table("pers_rel");

=head1 ACCESSORS

=head2 nr_person

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 per_nr_person

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 pers_reltyp

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 0

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_person",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "per_nr_person",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "pers_reltyp",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 0 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_person>

=item * L</per_nr_person>

=back

=cut

__PACKAGE__->set_primary_key("nr_person", "per_nr_person");

=head1 RELATIONS

=head2 nr_person_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Person>

=cut

__PACKAGE__->belongs_to(
  "nr_person_relation",
  "ZBW::Ifis::Schema::Result::Person",
  { nr_person => "nr_person" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 per_nr_person_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Person>

=cut

__PACKAGE__->belongs_to(
  "per_nr_person_relation",
  "ZBW::Ifis::Schema::Result::Person",
  { nr_person => "per_nr_person" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 pers_reltyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Reltyp>

=cut

__PACKAGE__->belongs_to(
  "pers_reltyp_relation",
  "ZBW::Ifis::Schema::Result::Reltyp",
  { nr_reltyp => "pers_reltyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:9nplDGtmuJspHFfBclRE5A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
