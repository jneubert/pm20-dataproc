use utf8;
package ZBW::Ifis::Schema::Result::Namen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Namen

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<namen>

=cut

__PACKAGE__->table("namen");

=head1 ACCESSORS

=head2 nr_name

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'namen_seq'

=head2 name_person

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 name_text

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 ppn_id

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 pnd_id

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 bis_id

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_name",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "namen_seq",
  },
  "name_person",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "name_text",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "ppn_id",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "pnd_id",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "bis_id",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_name>

=back

=cut

__PACKAGE__->set_primary_key("nr_name");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_namen_person>

=over 4

=item * L</name_person>

=back

=cut

__PACKAGE__->add_unique_constraint("u_namen_person", ["name_person"]);

=head1 RELATIONS

=head2 namen_syn_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::NamenSyn>

=cut

__PACKAGE__->has_many(
  "namen_syn_relations",
  "ZBW::Ifis::Schema::Result::NamenSyn",
  { "foreign.nr_name" => "self.nr_name" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Person>

=cut

__PACKAGE__->has_many(
  "person_relations",
  "ZBW::Ifis::Schema::Result::Person",
  { "foreign.nr_name" => "self.nr_name" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oyO2bb8P2YA2JCvbmp8Vpg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
