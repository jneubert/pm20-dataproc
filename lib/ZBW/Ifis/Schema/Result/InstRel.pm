use utf8;
package ZBW::Ifis::Schema::Result::InstRel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::InstRel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<inst_rel>

=cut

__PACKAGE__->table("inst_rel");

=head1 ACCESSORS

=head2 inst_reltyp

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_inst

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 ins_nr_inst

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "inst_reltyp",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 0 },
  "nr_inst",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "ins_nr_inst",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</inst_reltyp>

=item * L</nr_inst>

=item * L</ins_nr_inst>

=back

=cut

__PACKAGE__->set_primary_key("inst_reltyp", "nr_inst", "ins_nr_inst");

=head1 RELATIONS

=head2 ins_nr_inst_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->belongs_to(
  "ins_nr_inst_relation",
  "ZBW::Ifis::Schema::Result::Institution",
  { nr_inst => "ins_nr_inst" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 inst_reltyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Reltyp>

=cut

__PACKAGE__->belongs_to(
  "inst_reltyp_relation",
  "ZBW::Ifis::Schema::Result::Reltyp",
  { nr_reltyp => "inst_reltyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_inst_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->belongs_to(
  "nr_inst_relation",
  "ZBW::Ifis::Schema::Result::Institution",
  { nr_inst => "nr_inst" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ev3n3/eS6tH3pBm0RTj4Bw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
