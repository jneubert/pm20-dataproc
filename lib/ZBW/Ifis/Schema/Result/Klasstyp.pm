use utf8;
package ZBW::Ifis::Schema::Result::Klasstyp;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Klasstyp

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klasstyp>

=cut

__PACKAGE__->table("klasstyp");

=head1 ACCESSORS

=head2 nr_klasstyp

  data_type: 'smallint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'klasstyp_seq'

=head2 klass_code

  data_type: 'char'
  is_nullable: 0
  size: 2

=head2 klass_abk

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 klass_kz

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_klasstyp",
  {
    data_type         => "smallint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "klasstyp_seq",
  },
  "klass_code",
  { data_type => "char", is_nullable => 0, size => 2 },
  "klass_abk",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "klass_kz",
  { data_type => "char", is_nullable => 1, size => 2 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_klasstyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_klasstyp");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_klasstyp_code>

=over 4

=item * L</klass_code>

=back

=cut

__PACKAGE__->add_unique_constraint("u_klasstyp_code", ["klass_code"]);

=head1 RELATIONS

=head2 klassifikator_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->has_many(
  "klassifikator_relations",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { "foreign.klass_code" => "self.klass_code" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GYsdAOq7i0vYw05oA/BAxA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
