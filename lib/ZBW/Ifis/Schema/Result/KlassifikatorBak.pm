use utf8;
package ZBW::Ifis::Schema::Result::KlassifikatorBak;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::KlassifikatorBak

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klassifikator_bak>

=cut

__PACKAGE__->table("klassifikator_bak");

=head1 ACCESSORS

=head2 nr_klass

  data_type: 'bigint'
  is_nullable: 1

=head2 klass_not

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 name_term

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 name_term_en

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 klass_code

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 klass_iso_code

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 bis_id

  data_type: 'char'
  is_nullable: 1
  size: 10

=head2 ppn_id

  data_type: 'char'
  is_nullable: 1
  size: 15

=head2 klass_beschr_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_beschr_en

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_ausschl_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_ausschl_en

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_int_hinw

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_einschl

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_user

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stilldate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_norm

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_art

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_korrekturdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 gnd_id

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 klass_not_short

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 typ

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 komplett

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "nr_klass",
  { data_type => "bigint", is_nullable => 1 },
  "klass_not",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "name_term",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "name_term_en",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "klass_code",
  { data_type => "char", is_nullable => 1, size => 2 },
  "klass_iso_code",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "bis_id",
  { data_type => "char", is_nullable => 1, size => 10 },
  "ppn_id",
  { data_type => "char", is_nullable => 1, size => 15 },
  "klass_beschr_dt",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_beschr_en",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_ausschl_dt",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_ausschl_en",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_int_hinw",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_einschl",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_user",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "s_timestamp",
  { data_type => "timestamp", is_nullable => 1 },
  "s_firstdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stilldate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_norm",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_art",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_korrekturdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "gnd_id",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "klass_not_short",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "typ",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "komplett",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-12-16 10:19:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vbmWF6XM/UV4K1TFBLhnMw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
