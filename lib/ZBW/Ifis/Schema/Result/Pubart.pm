use utf8;
package ZBW::Ifis::Schema::Result::Pubart;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Pubart

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pubart>

=cut

__PACKAGE__->table("pubart");

=head1 ACCESSORS

=head2 nr_pubart

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'pubart_seq'

=head2 pubart_key

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 pubart_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_pubart",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "pubart_seq",
  },
  "pubart_key",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "pubart_dt",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_pubart>

=back

=cut

__PACKAGE__->set_primary_key("nr_pubart");

=head1 RELATIONS

=head2 pub_pubart_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubPubart>

=cut

__PACKAGE__->has_many(
  "pub_pubart_relations",
  "ZBW::Ifis::Schema::Result::PubPubart",
  { "foreign.nr_pubart" => "self.nr_pubart" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WmmEx0sXztX5tDaZODdKkw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
