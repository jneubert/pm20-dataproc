use utf8;
package ZBW::Ifis::Schema::Result::KlassRel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::KlassRel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klass_rel>

=cut

__PACKAGE__->table("klass_rel");

=head1 ACCESSORS

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 kla_nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 klass_reltyp

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 0

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "kla_nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "klass_reltyp",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 0 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_klass>

=item * L</kla_nr_klass>

=item * L</klass_reltyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_klass", "kla_nr_klass", "klass_reltyp");

=head1 RELATIONS

=head2 kla_nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "kla_nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "kla_nr_klass" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 klass_reltyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Reltyp>

=cut

__PACKAGE__->belongs_to(
  "klass_reltyp_relation",
  "ZBW::Ifis::Schema::Result::Reltyp",
  { nr_reltyp => "klass_reltyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0NAe6uT8JjK68uDtxRMcfQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
