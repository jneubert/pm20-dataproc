use utf8;
package ZBW::Ifis::Schema::Result::InstSyn;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::InstSyn

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<inst_syn>

=cut

__PACKAGE__->table("inst_syn");

=head1 ACCESSORS

=head2 nr_inst_syn

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'inst_syn_seq'

=head2 nr_inst

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 inst_syn

  data_type: 'varchar'
  is_nullable: 0
  size: 500

=head2 inst_syn_von_bis

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 inst_syn_gkd

  data_type: 'varchar'
  is_nullable: 1
  size: 11

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_inst_syn",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "inst_syn_seq",
  },
  "nr_inst",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "inst_syn",
  { data_type => "varchar", is_nullable => 0, size => 500 },
  "inst_syn_von_bis",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "inst_syn_gkd",
  { data_type => "varchar", is_nullable => 1, size => 11 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_inst_syn>

=back

=cut

__PACKAGE__->set_primary_key("nr_inst_syn");

=head1 UNIQUE CONSTRAINTS

=head2 C<inst_syn_unq>

=over 4

=item * L</nr_inst>

=item * L</inst_syn>

=back

=cut

__PACKAGE__->add_unique_constraint("inst_syn_unq", ["nr_inst", "inst_syn"]);

=head1 RELATIONS

=head2 nr_inst_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->belongs_to(
  "nr_inst_relation",
  "ZBW::Ifis::Schema::Result::Institution",
  { nr_inst => "nr_inst" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/N2F/T7JlkC4U5k3qXSCAA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
