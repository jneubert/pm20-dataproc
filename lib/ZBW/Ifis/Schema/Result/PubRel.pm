use utf8;
package ZBW::Ifis::Schema::Result::PubRel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PubRel

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pub_rel>

=cut

__PACKAGE__->table("pub_rel");

=head1 ACCESSORS

=head2 pub_reltyp

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_publikation

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 pub_nr_publikation

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "pub_reltyp",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 0 },
  "nr_publikation",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "pub_nr_publikation",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_publikation>

=item * L</pub_nr_publikation>

=item * L</pub_reltyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_publikation", "pub_nr_publikation", "pub_reltyp");

=head1 RELATIONS

=head2 nr_publikation_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publikation>

=cut

__PACKAGE__->belongs_to(
  "nr_publikation_relation",
  "ZBW::Ifis::Schema::Result::Publikation",
  { nr_publikation => "nr_publikation" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 pub_nr_publikation_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publikation>

=cut

__PACKAGE__->belongs_to(
  "pub_nr_publikation_relation",
  "ZBW::Ifis::Schema::Result::Publikation",
  { nr_publikation => "pub_nr_publikation" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 pub_reltyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Reltyp>

=cut

__PACKAGE__->belongs_to(
  "pub_reltyp_relation",
  "ZBW::Ifis::Schema::Result::Reltyp",
  { nr_reltyp => "pub_reltyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YO5xK1TwFe7RbAm3wbXJ1Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
