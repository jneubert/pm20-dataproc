use utf8;
package ZBW::Ifis::Schema::Result::Institution;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Institution

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<institution>

=cut

__PACKAGE__->table("institution");

=head1 ACCESSORS

=head2 nr_inst

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'institution_seq'

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 kla_nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 nr_rechtsform

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 inst_kennz

  data_type: 'char'
  is_nullable: 0
  size: 1

=head2 inst_name

  data_type: 'varchar'
  is_nullable: 0
  size: 500

=head2 bis_id

  data_type: 'char'
  is_nullable: 1
  size: 10

=head2 gkd_id

  data_type: 'char'
  is_nullable: 1
  size: 11

=head2 ppn_id

  data_type: 'char'
  is_nullable: 1
  size: 15

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 doss

  data_type: 'smallint'
  is_nullable: 1

=head2 inst_rak

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_abk

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 inst_ort

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 inst_adresse

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_erlaeut

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_bemerk

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_anf_ende

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 inst_pd_ausschn

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 inst_pd_bericht

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_pd_komm

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_enthalten

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 inst_verbind

  data_type: 'varchar'
  is_nullable: 1
  size: 750

=head2 inst_kontpers

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 inst_lief_az

  data_type: 'char'
  is_nullable: 1
  size: 10

=head2 konf_deadline

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 konf_komm

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 konf_zaehl

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 konf_anf

  data_type: 'timestamp'
  is_nullable: 1

=head2 konf_ende

  data_type: 'timestamp'
  is_nullable: 1

=head2 firmsign

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 firmsiga

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_import

  data_type: 'text'
  is_nullable: 1

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_stilldate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_norm

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_art

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_korr

  data_type: 'char'
  is_nullable: 1
  size: 1

=cut

__PACKAGE__->add_columns(
  "nr_inst",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "institution_seq",
  },
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "kla_nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "nr_rechtsform",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "inst_kennz",
  { data_type => "char", is_nullable => 0, size => 1 },
  "inst_name",
  { data_type => "varchar", is_nullable => 0, size => 500 },
  "bis_id",
  { data_type => "char", is_nullable => 1, size => 10 },
  "gkd_id",
  { data_type => "char", is_nullable => 1, size => 11 },
  "ppn_id",
  { data_type => "char", is_nullable => 1, size => 15 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "doss",
  { data_type => "smallint", is_nullable => 1 },
  "inst_rak",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_abk",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "inst_ort",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "inst_adresse",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_erlaeut",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_bemerk",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_anf_ende",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "inst_pd_ausschn",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "inst_pd_bericht",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_pd_komm",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_enthalten",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "inst_verbind",
  { data_type => "varchar", is_nullable => 1, size => 750 },
  "inst_kontpers",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "inst_lief_az",
  { data_type => "char", is_nullable => 1, size => 10 },
  "konf_deadline",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "konf_komm",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "konf_zaehl",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "konf_anf",
  { data_type => "timestamp", is_nullable => 1 },
  "konf_ende",
  { data_type => "timestamp", is_nullable => 1 },
  "firmsign",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "firmsiga",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
  "s_import",
  { data_type => "text", is_nullable => 1 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_stilldate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_norm",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_art",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_korr",
  { data_type => "char", is_nullable => 1, size => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_inst>

=back

=cut

__PACKAGE__->set_primary_key("nr_inst");

=head1 RELATIONS

=head2 inst_funkpers_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstFunkpers>

=cut

__PACKAGE__->has_many(
  "inst_funkpers_relations",
  "ZBW::Ifis::Schema::Result::InstFunkpers",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_ag_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassAg>

=cut

__PACKAGE__->has_many(
  "inst_klass_ag_relations",
  "ZBW::Ifis::Schema::Result::InstKlassAg",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_gk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassGk>

=cut

__PACKAGE__->has_many(
  "inst_klass_gk_relations",
  "ZBW::Ifis::Schema::Result::InstKlassGk",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_na_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassNa>

=cut

__PACKAGE__->has_many(
  "inst_klass_na_relations",
  "ZBW::Ifis::Schema::Result::InstKlassNa",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_sach_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSach>

=cut

__PACKAGE__->has_many(
  "inst_klass_sach_relations",
  "ZBW::Ifis::Schema::Result::InstKlassSach",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_sk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSk>

=cut

__PACKAGE__->has_many(
  "inst_klass_sk_relations",
  "ZBW::Ifis::Schema::Result::InstKlassSk",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_ware_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassWare>

=cut

__PACKAGE__->has_many(
  "inst_klass_ware_relations",
  "ZBW::Ifis::Schema::Result::InstKlassWare",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstQuelle>

=cut

__PACKAGE__->has_many(
  "inst_quelle_relations",
  "ZBW::Ifis::Schema::Result::InstQuelle",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_rel_ins_nr_inst_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstRel>

=cut

__PACKAGE__->has_many(
  "inst_rel_ins_nr_inst_relations",
  "ZBW::Ifis::Schema::Result::InstRel",
  { "foreign.ins_nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_rel_nr_inst_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstRel>

=cut

__PACKAGE__->has_many(
  "inst_rel_nr_inst_relations",
  "ZBW::Ifis::Schema::Result::InstRel",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_syn_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstSyn>

=cut

__PACKAGE__->has_many(
  "inst_syn_relations",
  "ZBW::Ifis::Schema::Result::InstSyn",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstText>

=cut

__PACKAGE__->has_many(
  "inst_text_relations",
  "ZBW::Ifis::Schema::Result::InstText",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_typ_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstTyp>

=cut

__PACKAGE__->has_many(
  "inst_typ_relations",
  "ZBW::Ifis::Schema::Result::InstTyp",
  { "foreign.nr_inst" => "self.nr_inst" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 kla_nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "kla_nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "kla_nr_klass" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 nr_rechtsform_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Rechtsform>

=cut

__PACKAGE__->belongs_to(
  "nr_rechtsform_relation",
  "ZBW::Ifis::Schema::Result::Rechtsform",
  { nr_rechtsform => "nr_rechtsform" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yPin3Ulpl8L0OSIWAhxYVQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration

=head2 insttyp_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Insttyp>

Bridges L</inst_typ_relations> and
L</nr_insttyp_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'insttyp_relations', 'inst_typ_relations', 'nr_insttyp_relation' );



=head2 instklass_sk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSk>

Bridges L</inst_klass_sk_relations> and
L</nr_klass_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'instklass_sk_relations', 'inst_klass_sk_relations', 'nr_klass_relation' );



=head2 instklass_gk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassGk>

Bridges L</inst_klass_gk_relations> and
L</nr_klass_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'instklass_gk_relations', 'inst_klass_gk_relations', 'nr_klass_relation' );



=head2 instklass_na_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassNa>

Bridges L</inst_klass_na_relations> and
L</nr_klass_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'instklass_na_relations', 'inst_klass_na_relations', 'nr_klass_relation' );



=head2 inst_inst_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstRel>

Bridges L</inst_rel_nr_inst_relations> and
L</ins_nr_inst_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'inst_inst_relations', 'inst_rel_nr_inst_relations', 'ins_nr_inst_relation' );



=head2 insttext_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Texttypen>

Bridges L</inst_text_relations> and
L</nr_texttyp_relation>. For all instances,

=cut

__PACKAGE__->many_to_many( 'insttext_relations', 'inst_text_relations', 'nr_texttyp_relation' );




1;
