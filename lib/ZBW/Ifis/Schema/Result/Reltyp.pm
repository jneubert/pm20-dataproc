use utf8;
package ZBW::Ifis::Schema::Result::Reltyp;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Reltyp

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<reltyp>

=cut

__PACKAGE__->table("reltyp");

=head1 ACCESSORS

=head2 nr_reltyp

  data_type: 'smallint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'reltyp_seq'

=head2 abk1_dt

  data_type: 'char'
  is_nullable: 0
  size: 7

=head2 abk2_dt

  data_type: 'char'
  is_nullable: 0
  size: 7

=head2 abk1_en

  data_type: 'char'
  is_nullable: 1
  size: 7

=head2 abk2_en

  data_type: 'char'
  is_nullable: 1
  size: 7

=head2 bez1_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 bez2_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 bez1_en

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 bez2_en

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 rel_klass

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_reltyp",
  {
    data_type         => "smallint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "reltyp_seq",
  },
  "abk1_dt",
  { data_type => "char", is_nullable => 0, size => 7 },
  "abk2_dt",
  { data_type => "char", is_nullable => 0, size => 7 },
  "abk1_en",
  { data_type => "char", is_nullable => 1, size => 7 },
  "abk2_en",
  { data_type => "char", is_nullable => 1, size => 7 },
  "bez1_dt",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "bez2_dt",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "bez1_en",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "bez2_en",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "rel_klass",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_reltyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_reltyp");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_reltyp_abk1_dt>

=over 4

=item * L</abk1_dt>

=item * L</rel_klass>

=back

=cut

__PACKAGE__->add_unique_constraint("u_reltyp_abk1_dt", ["abk1_dt", "rel_klass"]);

=head2 C<u_reltyp_abk1_en>

=over 4

=item * L</abk1_en>

=item * L</rel_klass>

=back

=cut

__PACKAGE__->add_unique_constraint("u_reltyp_abk1_en", ["abk1_en", "rel_klass"]);

=head2 C<u_reltyp_abk2_dt>

=over 4

=item * L</abk2_dt>

=item * L</rel_klass>

=back

=cut

__PACKAGE__->add_unique_constraint("u_reltyp_abk2_dt", ["abk2_dt", "rel_klass"]);

=head2 C<u_reltyp_abk2_en>

=over 4

=item * L</abk2_en>

=item * L</rel_klass>

=back

=cut

__PACKAGE__->add_unique_constraint("u_reltyp_abk2_en", ["abk2_en", "rel_klass"]);

=head1 RELATIONS

=head2 inst_rel_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstRel>

=cut

__PACKAGE__->has_many(
  "inst_rel_relations",
  "ZBW::Ifis::Schema::Result::InstRel",
  { "foreign.inst_reltyp" => "self.nr_reltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_rel_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassRel>

=cut

__PACKAGE__->has_many(
  "klass_rel_relations",
  "ZBW::Ifis::Schema::Result::KlassRel",
  { "foreign.klass_reltyp" => "self.nr_reltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_rel_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersRel>

=cut

__PACKAGE__->has_many(
  "pers_rel_relations",
  "ZBW::Ifis::Schema::Result::PersRel",
  { "foreign.pers_reltyp" => "self.nr_reltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_rel_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubRel>

=cut

__PACKAGE__->has_many(
  "pub_rel_relations",
  "ZBW::Ifis::Schema::Result::PubRel",
  { "foreign.pub_reltyp" => "self.nr_reltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AfBZgGV0C/eVLgmMJCmSNw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
