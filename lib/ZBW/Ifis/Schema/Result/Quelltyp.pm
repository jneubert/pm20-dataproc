use utf8;
package ZBW::Ifis::Schema::Result::Quelltyp;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Quelltyp

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<quelltyp>

=cut

__PACKAGE__->table("quelltyp");

=head1 ACCESSORS

=head2 nr_quelltyp

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'quelltyp_seq'

=head2 quelltyp_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 500

=head2 quelltyp_en

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_quelltyp",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "quelltyp_seq",
  },
  "quelltyp_dt",
  { data_type => "varchar", is_nullable => 0, size => 500 },
  "quelltyp_en",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_quelltyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_quelltyp");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_quelltyp_dt>

=over 4

=item * L</quelltyp_dt>

=back

=cut

__PACKAGE__->add_unique_constraint("u_quelltyp_dt", ["quelltyp_dt"]);

=head1 RELATIONS

=head2 inst_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstQuelle>

=cut

__PACKAGE__->has_many(
  "inst_quelle_relations",
  "ZBW::Ifis::Schema::Result::InstQuelle",
  { "foreign.nr_quelltyp" => "self.nr_quelltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersQuelle>

=cut

__PACKAGE__->has_many(
  "pers_quelle_relations",
  "ZBW::Ifis::Schema::Result::PersQuelle",
  { "foreign.nr_quelltyp" => "self.nr_quelltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubQuelle>

=cut

__PACKAGE__->has_many(
  "pub_quelle_relations",
  "ZBW::Ifis::Schema::Result::PubQuelle",
  { "foreign.nr_quelltyp" => "self.nr_quelltyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UND5OY/pIOg5M+cU1pfVrQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
