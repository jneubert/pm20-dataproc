use utf8;
package ZBW::Ifis::Schema::Result::Klassifikator;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Klassifikator

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klassifikator>

=cut

__PACKAGE__->table("klassifikator");

=head1 ACCESSORS

=head2 nr_klass

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'klassifikator_seq'

=head2 klass_not

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 name_term

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 name_term_en

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 klass_code

  data_type: 'char'
  is_foreign_key: 1
  is_nullable: 0
  size: 2

=head2 klass_iso_code

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 bis_id

  data_type: 'char'
  is_nullable: 1
  size: 10

=head2 ppn_id

  data_type: 'char'
  is_nullable: 1
  size: 15

=head2 klass_beschr_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_beschr_en

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_ausschl_dt

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_ausschl_en

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_int_hinw

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 klass_einschl

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_stilldate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_norm

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_art

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 s_korrekturdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 gnd_id

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 klass_not_short

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 30

=head2 typ

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 komplett

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 mappen_anzahl

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_klass",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "klassifikator_seq",
  },
  "klass_not",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "name_term",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "name_term_en",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "klass_code",
  { data_type => "char", is_foreign_key => 1, is_nullable => 0, size => 2 },
  "klass_iso_code",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "bis_id",
  { data_type => "char", is_nullable => 1, size => 10 },
  "ppn_id",
  { data_type => "char", is_nullable => 1, size => 15 },
  "klass_beschr_dt",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_beschr_en",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_ausschl_dt",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_ausschl_en",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_int_hinw",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "klass_einschl",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_stilldate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_norm",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_art",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "s_korrekturdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "gnd_id",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "klass_not_short",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 30 },
  "typ",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "komplett",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "mappen_anzahl",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_klass>

=back

=cut

__PACKAGE__->set_primary_key("nr_klass");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_klassif_code_term>

=over 4

=item * L</klass_code>

=item * L</klass_not>

=item * L</name_term>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "u_klassif_code_term",
  ["klass_code", "klass_not", "name_term"],
);

=head2 C<u_klassif_not>

=over 4

=item * L</klass_not>

=back

=cut

__PACKAGE__->add_unique_constraint("u_klassif_not", ["klass_not"]);

=head2 C<u_klassif_not_short>

=over 4

=item * L</klass_not_short>

=back

=cut

__PACKAGE__->add_unique_constraint("u_klassif_not_short", ["klass_not_short"]);

=head1 RELATIONS

=head2 inst_klass_ag_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassAg>

=cut

__PACKAGE__->has_many(
  "inst_klass_ag_relations",
  "ZBW::Ifis::Schema::Result::InstKlassAg",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_gk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassGk>

=cut

__PACKAGE__->has_many(
  "inst_klass_gk_relations",
  "ZBW::Ifis::Schema::Result::InstKlassGk",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_na_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassNa>

=cut

__PACKAGE__->has_many(
  "inst_klass_na_relations",
  "ZBW::Ifis::Schema::Result::InstKlassNa",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_sach_nr_klass_ag_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSach>

=cut

__PACKAGE__->has_many(
  "inst_klass_sach_nr_klass_ag_relations",
  "ZBW::Ifis::Schema::Result::InstKlassSach",
  { "foreign.nr_klass_ag" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_sach_nr_klass_je_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSach>

=cut

__PACKAGE__->has_many(
  "inst_klass_sach_nr_klass_je_relations",
  "ZBW::Ifis::Schema::Result::InstKlassSach",
  { "foreign.nr_klass_je" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_sk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassSk>

=cut

__PACKAGE__->has_many(
  "inst_klass_sk_relations",
  "ZBW::Ifis::Schema::Result::InstKlassSk",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_ware_nr_klass_ag_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassWare>

=cut

__PACKAGE__->has_many(
  "inst_klass_ware_nr_klass_ag_relations",
  "ZBW::Ifis::Schema::Result::InstKlassWare",
  { "foreign.nr_klass_ag" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_klass_ware_nr_klass_ip_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstKlassWare>

=cut

__PACKAGE__->has_many(
  "inst_klass_ware_nr_klass_ip_relations",
  "ZBW::Ifis::Schema::Result::InstKlassWare",
  { "foreign.nr_klass_ip" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 inst_syn_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstSyn>

=cut

__PACKAGE__->has_many(
  "inst_syn_relations",
  "ZBW::Ifis::Schema::Result::InstSyn",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 institution_kla_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->has_many(
  "institution_kla_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::Institution",
  { "foreign.kla_nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 institution_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->has_many(
  "institution_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::Institution",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_code_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klasstyp>

=cut

__PACKAGE__->belongs_to(
  "klass_code_relation",
  "ZBW::Ifis::Schema::Result::Klasstyp",
  { klass_code => "klass_code" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 klass_rel_je_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassRelJe>

=cut

__PACKAGE__->has_many(
  "klass_rel_je_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::KlassRelJe",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_rel_je_sup_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassRelJe>

=cut

__PACKAGE__->has_many(
  "klass_rel_je_sup_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::KlassRelJe",
  { "foreign.sup_nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_rel_kla_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassRel>

=cut

__PACKAGE__->has_many(
  "klass_rel_kla_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::KlassRel",
  { "foreign.kla_nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_rel_nr_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassRel>

=cut

__PACKAGE__->has_many(
  "klass_rel_nr_klass_relations",
  "ZBW::Ifis::Schema::Result::KlassRel",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassText>

=cut

__PACKAGE__->has_many(
  "klass_text_relations",
  "ZBW::Ifis::Schema::Result::KlassText",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_klass_sach_nr_klass_ag_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersKlassSach>

=cut

__PACKAGE__->has_many(
  "pers_klass_sach_nr_klass_ag_relations",
  "ZBW::Ifis::Schema::Result::PersKlassSach",
  { "foreign.nr_klass_ag" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_klass_sach_nr_klass_je_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersKlassSach>

=cut

__PACKAGE__->has_many(
  "pers_klass_sach_nr_klass_je_relations",
  "ZBW::Ifis::Schema::Result::PersKlassSach",
  { "foreign.nr_klass_je" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_wirk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersWirk>

=cut

__PACKAGE__->has_many(
  "pers_wirk_relations",
  "ZBW::Ifis::Schema::Result::PersWirk",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Person>

=cut

__PACKAGE__->has_many(
  "person_relations",
  "ZBW::Ifis::Schema::Result::Person",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubKlass>

=cut

__PACKAGE__->has_many(
  "pub_klass_relations",
  "ZBW::Ifis::Schema::Result::PubKlass",
  { "foreign.nr_klass" => "self.nr_klass" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 nr_klass_relation_relations

Type: many_to_many

Composing rels: L</klass_rel_je_sup_nr_klass_relations> -> nr_klass_relation

=cut

__PACKAGE__->many_to_many(
  "nr_klass_relation_relations",
  "klass_rel_je_sup_nr_klass_relations",
  "nr_klass_relation",
);

=head2 sup_nr_klass_relation_relations

Type: many_to_many

Composing rels: L</klass_rel_je_nr_klass_relations> -> sup_nr_klass_relation

=cut

__PACKAGE__->many_to_many(
  "sup_nr_klass_relation_relations",
  "klass_rel_je_nr_klass_relations",
  "sup_nr_klass_relation",
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-12-28 10:49:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RoapMZnsm8FjuA5pHtFP8w


# You can replace this text with custom code or comments, and it will be preserved on regeneration

# relations general
__PACKAGE__->many_to_many( "klass_broader_relations" => "klass_rel_nr_klass_relations", "kla_nr_klass_relation" );
__PACKAGE__->many_to_many( "klass_narrower_relations" => "klass_rel_kla_nr_klass_relations", "nr_klass_relation" );

# newly created hierarchy for JE
__PACKAGE__->many_to_many( "klass_broader_je_relations" => "klass_rel_je_nr_klass_relations", "sup_nr_klass_relation" );
__PACKAGE__->many_to_many( "klass_narrower_je_relations" => "klass_rel_je_sup_nr_klass_relations", "nr_klass_relation" );

1;
