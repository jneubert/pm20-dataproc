use utf8;
package ZBW::Ifis::Schema::Result::PubUrl;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PubUrl

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pub_url>

=cut

__PACKAGE__->table("pub_url");

=head1 ACCESSORS

=head2 nr_pub_url

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'pub_url_seq'

=head2 nr_publikation

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_publizenz

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 pub_url

  data_type: 'varchar'
  is_nullable: 1
  size: 1000

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_pub_url",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "pub_url_seq",
  },
  "nr_publikation",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_publizenz",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "pub_url",
  { data_type => "varchar", is_nullable => 1, size => 1000 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_pub_url>

=back

=cut

__PACKAGE__->set_primary_key("nr_pub_url");

=head1 RELATIONS

=head2 nr_publikation_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publikation>

=cut

__PACKAGE__->belongs_to(
  "nr_publikation_relation",
  "ZBW::Ifis::Schema::Result::Publikation",
  { nr_publikation => "nr_publikation" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_publizenz_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publizenz>

=cut

__PACKAGE__->belongs_to(
  "nr_publizenz_relation",
  "ZBW::Ifis::Schema::Result::Publizenz",
  { nr_publizenz => "nr_publizenz" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Am6Cuuzzx1yNQlq/+2e1jw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
