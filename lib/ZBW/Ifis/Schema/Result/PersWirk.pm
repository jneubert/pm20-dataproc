use utf8;
package ZBW::Ifis::Schema::Result::PersWirk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PersWirk

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pers_wirk>

=cut

__PACKAGE__->table("pers_wirk");

=head1 ACCESSORS

=head2 nr_wirkbereich

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_person

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 wirk_von_bis

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 klass_code

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_wirkbereich",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_person",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "wirk_von_bis",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "klass_code",
  { data_type => "char", is_nullable => 1, size => 2 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_wirkbereich>

=item * L</nr_person>

=item * L</nr_klass>

=back

=cut

__PACKAGE__->set_primary_key("nr_wirkbereich", "nr_person", "nr_klass");

=head1 RELATIONS

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_person_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Person>

=cut

__PACKAGE__->belongs_to(
  "nr_person_relation",
  "ZBW::Ifis::Schema::Result::Person",
  { nr_person => "nr_person" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 nr_wirkbereich_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Wirkbereich>

=cut

__PACKAGE__->belongs_to(
  "nr_wirkbereich_relation",
  "ZBW::Ifis::Schema::Result::Wirkbereich",
  { nr_wirkbereich => "nr_wirkbereich" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wEcQagkCzqNOK2VFZE+MvQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
