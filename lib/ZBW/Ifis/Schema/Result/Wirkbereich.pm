use utf8;
package ZBW::Ifis::Schema::Result::Wirkbereich;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Wirkbereich

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<wirkbereich>

=cut

__PACKAGE__->table("wirkbereich");

=head1 ACCESSORS

=head2 nr_wirkbereich

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'wirkbereich_seq'

=head2 wirkbereich_abk

  data_type: 'char'
  is_nullable: 0
  size: 3

=head2 wirkbereich_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 wirkbereich_en

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_wirkbereich",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "wirkbereich_seq",
  },
  "wirkbereich_abk",
  { data_type => "char", is_nullable => 0, size => 3 },
  "wirkbereich_dt",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "wirkbereich_en",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_wirkbereich>

=back

=cut

__PACKAGE__->set_primary_key("nr_wirkbereich");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_wirkbereich_abk>

=over 4

=item * L</wirkbereich_abk>

=back

=cut

__PACKAGE__->add_unique_constraint("u_wirkbereich_abk", ["wirkbereich_abk"]);

=head2 C<u_wirkbereich_dt>

=over 4

=item * L</wirkbereich_dt>

=back

=cut

__PACKAGE__->add_unique_constraint("u_wirkbereich_dt", ["wirkbereich_dt"]);

=head1 RELATIONS

=head2 pers_wirk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersWirk>

=cut

__PACKAGE__->has_many(
  "pers_wirk_relations",
  "ZBW::Ifis::Schema::Result::PersWirk",
  { "foreign.nr_wirkbereich" => "self.nr_wirkbereich" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bWWeHrD6ntChY7o65+JMfg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
