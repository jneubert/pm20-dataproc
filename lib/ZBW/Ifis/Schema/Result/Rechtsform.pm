use utf8;
package ZBW::Ifis::Schema::Result::Rechtsform;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Rechtsform

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<rechtsform>

=cut

__PACKAGE__->table("rechtsform");

=head1 ACCESSORS

=head2 nr_rechtsform

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'rechtsform_seq'

=head2 rechtsform_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_rechtsform",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "rechtsform_seq",
  },
  "rechtsform_dt",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_rechtsform>

=back

=cut

__PACKAGE__->set_primary_key("nr_rechtsform");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_rechtsform_dt>

=over 4

=item * L</rechtsform_dt>

=back

=cut

__PACKAGE__->add_unique_constraint("u_rechtsform_dt", ["rechtsform_dt"]);

=head1 RELATIONS

=head2 institution_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Institution>

=cut

__PACKAGE__->has_many(
  "institution_relations",
  "ZBW::Ifis::Schema::Result::Institution",
  { "foreign.nr_rechtsform" => "self.nr_rechtsform" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fNldlQX8LJZqvoBZvfIRUw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
