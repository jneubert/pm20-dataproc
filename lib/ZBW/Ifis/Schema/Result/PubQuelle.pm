use utf8;
package ZBW::Ifis::Schema::Result::PubQuelle;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::PubQuelle

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<pub_quelle>

=cut

__PACKAGE__->table("pub_quelle");

=head1 ACCESSORS

=head2 nr_pub_quelle

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'pub_quelle_seq'

=head2 nr_publikation

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_quelltyp

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 pub_quelle_kom

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_pub_quelle",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "pub_quelle_seq",
  },
  "nr_publikation",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_quelltyp",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "pub_quelle_kom",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_pub_quelle>

=back

=cut

__PACKAGE__->set_primary_key("nr_pub_quelle");

=head1 RELATIONS

=head2 nr_publikation_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Publikation>

=cut

__PACKAGE__->belongs_to(
  "nr_publikation_relation",
  "ZBW::Ifis::Schema::Result::Publikation",
  { nr_publikation => "nr_publikation" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_quelltyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Quelltyp>

=cut

__PACKAGE__->belongs_to(
  "nr_quelltyp_relation",
  "ZBW::Ifis::Schema::Result::Quelltyp",
  { nr_quelltyp => "nr_quelltyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JXYz8KW/do6X650gJnA3Ow


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
