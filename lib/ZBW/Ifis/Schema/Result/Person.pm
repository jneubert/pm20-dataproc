use utf8;
package ZBW::Ifis::Schema::Result::Person;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Person

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<person>

=cut

__PACKAGE__->table("person");

=head1 ACCESSORS

=head2 nr_person

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'person_seq'

=head2 nr_name

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_akgrad

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 name_person

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 bis_id

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 ppn_id

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 pnd_id

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 pers_pd_ausschn

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pers_pd_komm

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pers_enthalten

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pers_beruf

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pers_bemerk

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pers_lebdat

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 geb_dat

  data_type: 'timestamp'
  is_nullable: 1

=head2 tod_dat

  data_type: 'timestamp'
  is_nullable: 1

=head2 nobel

  data_type: 'smallint'
  is_nullable: 1

=head2 doss

  data_type: 'smallint'
  is_nullable: 1

=head2 s_import

  data_type: 'text'
  is_nullable: 1

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_stilldate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_norm

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_art

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_korr

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "nr_person",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "person_seq",
  },
  "nr_name",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_akgrad",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "name_person",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "bis_id",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "ppn_id",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "pnd_id",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "pers_pd_ausschn",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pers_pd_komm",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pers_enthalten",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pers_beruf",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pers_bemerk",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pers_lebdat",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "geb_dat",
  { data_type => "timestamp", is_nullable => 1 },
  "tod_dat",
  { data_type => "timestamp", is_nullable => 1 },
  "nobel",
  { data_type => "smallint", is_nullable => 1 },
  "doss",
  { data_type => "smallint", is_nullable => 1 },
  "s_import",
  { data_type => "text", is_nullable => 1 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_stilldate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_norm",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_art",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_korr",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_person>

=back

=cut

__PACKAGE__->set_primary_key("nr_person");

=head1 RELATIONS

=head2 inst_funkpers_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstFunkpers>

=cut

__PACKAGE__->has_many(
  "inst_funkpers_relations",
  "ZBW::Ifis::Schema::Result::InstFunkpers",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 nr_akgrad_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Akgrad>

=cut

__PACKAGE__->belongs_to(
  "nr_akgrad_relation",
  "ZBW::Ifis::Schema::Result::Akgrad",
  { nr_akgrad => "nr_akgrad" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 nr_name_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Namen>

=cut

__PACKAGE__->belongs_to(
  "nr_name_relation",
  "ZBW::Ifis::Schema::Result::Namen",
  { nr_name => "nr_name" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 pers_klass_sach_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersKlassSach>

=cut

__PACKAGE__->has_many(
  "pers_klass_sach_relations",
  "ZBW::Ifis::Schema::Result::PersKlassSach",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersQuelle>

=cut

__PACKAGE__->has_many(
  "pers_quelle_relations",
  "ZBW::Ifis::Schema::Result::PersQuelle",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_rel_nr_person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersRel>

=cut

__PACKAGE__->has_many(
  "pers_rel_nr_person_relations",
  "ZBW::Ifis::Schema::Result::PersRel",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_rel_per_nr_person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersRel>

=cut

__PACKAGE__->has_many(
  "pers_rel_per_nr_person_relations",
  "ZBW::Ifis::Schema::Result::PersRel",
  { "foreign.per_nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersText>

=cut

__PACKAGE__->has_many(
  "pers_text_relations",
  "ZBW::Ifis::Schema::Result::PersText",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_wirk_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersWirk>

=cut

__PACKAGE__->has_many(
  "pers_wirk_relations",
  "ZBW::Ifis::Schema::Result::PersWirk",
  { "foreign.nr_person" => "self.nr_person" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1lUpcMQp+6hew60DAo5apw


# You can replace this text with custom code or comments, and it will be preserved on regeneration

=head1 CUSTOM RELATIONS

=head2 person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Person>

Bridges L</pers_rel_nr_person_relations> and
L</pers_rel_per_nr_person_relations>. For all instances,
C<pers_rel.pers_reltyp> is 4 (related) and can be ignored.

=cut

__PACKAGE__->many_to_many( 'person_relations', 'pers_rel_nr_person_relations', 'per_nr_person_relation' );


=head2 inverse_person_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Person>

Bridges L</pers_rel_nr_person_relations> and
L</pers_rel_per_nr_person_relations>. For all instances,
C<pers_rel.pers_reltyp> is 4 (related) and can be ignored.

=cut

__PACKAGE__->many_to_many( 'inverse_person_relations', 'pers_rel_per_nr_person_relations', 'nr_person_relation' );


=head2 person_synonyms

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::NamenSyn>

Bridges L</nr_name_relation> and L</namen_syn_relations>.

=cut

__PACKAGE__->many_to_many( 'person_synonyms', 'nr_name_relation', 'namen_syn_relations' );


=head2 quelltyp_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::Quelltyp>

Bridges L</pers_quelle_relations> and
L</nr_quelltyp_relations>.

=cut

__PACKAGE__->many_to_many( 'quelltyp_relations', 'pers_quelle_relations', 'nr_quelltyp_relation' );


1;
