use utf8;
package ZBW::Ifis::Schema::Result::KlassText;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::KlassText

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<klass_text>

=cut

__PACKAGE__->table("klass_text");

=head1 ACCESSORS

=head2 nr_klass_text

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'klass_text_seq'

=head2 nr_texttyp

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_klass

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 0

=head2 nr_sprache

  data_type: 'smallint'
  is_foreign_key: 1
  is_nullable: 1

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 1000

=head2 text

  data_type: 'text'
  is_nullable: 1

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_art

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_versdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_savedat

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_korrekturdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=cut

__PACKAGE__->add_columns(
  "nr_klass_text",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "klass_text_seq",
  },
  "nr_texttyp",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_klass",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 0 },
  "nr_sprache",
  { data_type => "smallint", is_foreign_key => 1, is_nullable => 1 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 1000 },
  "text",
  { data_type => "text", is_nullable => 1 },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_art",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_versdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_savedat",
  { data_type => "timestamp", is_nullable => 1 },
  "s_korrekturdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_klass_text>

=back

=cut

__PACKAGE__->set_primary_key("nr_klass_text");

=head1 RELATIONS

=head2 nr_klass_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Klassifikator>

=cut

__PACKAGE__->belongs_to(
  "nr_klass_relation",
  "ZBW::Ifis::Schema::Result::Klassifikator",
  { nr_klass => "nr_klass" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 nr_sprache_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Sprache>

=cut

__PACKAGE__->belongs_to(
  "nr_sprache_relation",
  "ZBW::Ifis::Schema::Result::Sprache",
  { nr_sprache => "nr_sprache" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 nr_texttyp_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Texttypen>

=cut

__PACKAGE__->belongs_to(
  "nr_texttyp_relation",
  "ZBW::Ifis::Schema::Result::Texttypen",
  { nr_texttyp => "nr_texttyp" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:d56/5DtSqCYU4hA3LYsY5A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
