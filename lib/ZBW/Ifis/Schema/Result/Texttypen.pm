use utf8;
package ZBW::Ifis::Schema::Result::Texttypen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Texttypen

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<texttypen>

=cut

__PACKAGE__->table("texttypen");

=head1 ACCESSORS

=head2 s_loesch

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 nr_texttyp

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'texttypen_seq'

=head2 texttyp_kz

  data_type: 'char'
  is_nullable: 0
  size: 2

=head2 texttyp_key

  data_type: 'char'
  is_nullable: 0
  size: 4

=head2 texttyp_dt

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 texttyp_en

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "s_loesch",
  { data_type => "char", is_nullable => 1, size => 1 },
  "nr_texttyp",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "texttypen_seq",
  },
  "texttyp_kz",
  { data_type => "char", is_nullable => 0, size => 2 },
  "texttyp_key",
  { data_type => "char", is_nullable => 0, size => 4 },
  "texttyp_dt",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "texttyp_en",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_texttyp>

=back

=cut

__PACKAGE__->set_primary_key("nr_texttyp");

=head1 UNIQUE CONSTRAINTS

=head2 C<u_texttypen_kz_key>

=over 4

=item * L</texttyp_kz>

=item * L</texttyp_key>

=back

=cut

__PACKAGE__->add_unique_constraint("u_texttypen_kz_key", ["texttyp_kz", "texttyp_key"]);

=head2 C<u_texttypen_kz_key_dt>

=over 4

=item * L</texttyp_kz>

=item * L</texttyp_key>

=item * L</texttyp_dt>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "u_texttypen_kz_key_dt",
  ["texttyp_kz", "texttyp_key", "texttyp_dt"],
);

=head1 RELATIONS

=head2 inst_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::InstText>

=cut

__PACKAGE__->has_many(
  "inst_text_relations",
  "ZBW::Ifis::Schema::Result::InstText",
  { "foreign.nr_texttyp" => "self.nr_texttyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 klass_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::KlassText>

=cut

__PACKAGE__->has_many(
  "klass_text_relations",
  "ZBW::Ifis::Schema::Result::KlassText",
  { "foreign.nr_texttyp" => "self.nr_texttyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pers_text_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PersText>

=cut

__PACKAGE__->has_many(
  "pers_text_relations",
  "ZBW::Ifis::Schema::Result::PersText",
  { "foreign.nr_texttyp" => "self.nr_texttyp" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gPIk3GTnyAKZHeqI4VU81A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
