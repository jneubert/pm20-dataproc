use utf8;
package ZBW::Ifis::Schema::Result::Publikation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

ZBW::Ifis::Schema::Result::Publikation

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 TABLE: C<publikation>

=cut

__PACKAGE__->table("publikation");

=head1 ACCESSORS

=head2 nr_publikation

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'publikation_seq'

=head2 nr_sprache

  data_type: 'bigint'
  is_foreign_key: 1
  is_nullable: 1

=head2 old_nr_publ

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 pub_titel

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 pub_titel_voll

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pub_bemerk

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pub_ort

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 zdb_id

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 ppn_id

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 pub_bstd

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 pub_bstd_beg

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 pub_bstd_end

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 pub_bstd_spot

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 pub_single

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_timestamp

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_firstdate

  data_type: 'timestamp'
  default_value: ('now'::text)::timestamp without time zone
  is_nullable: 1

=head2 s_importdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_exportdate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_savedate

  data_type: 'timestamp'
  is_nullable: 1

=head2 s_stat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_frei

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 s_user

  data_type: 'varchar'
  default_value: 'nbt'
  is_nullable: 1
  size: 30

=cut

__PACKAGE__->add_columns(
  "nr_publikation",
  {
    data_type         => "bigint",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "publikation_seq",
  },
  "nr_sprache",
  { data_type => "bigint", is_foreign_key => 1, is_nullable => 1 },
  "old_nr_publ",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "pub_titel",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "pub_titel_voll",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pub_bemerk",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pub_ort",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "zdb_id",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "ppn_id",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "pub_bstd",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "pub_bstd_beg",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "pub_bstd_end",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "pub_bstd_spot",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "pub_single",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_timestamp",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_firstdate",
  {
    data_type     => "timestamp",
    default_value => \"('now'::text)::timestamp without time zone",
    is_nullable   => 1,
  },
  "s_importdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_exportdate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_savedate",
  { data_type => "timestamp", is_nullable => 1 },
  "s_stat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_frei",
  { data_type => "char", is_nullable => 1, size => 1 },
  "s_user",
  {
    data_type => "varchar",
    default_value => "nbt",
    is_nullable => 1,
    size => 30,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</nr_publikation>

=back

=cut

__PACKAGE__->set_primary_key("nr_publikation");

=head1 RELATIONS

=head2 nr_sprache_relation

Type: belongs_to

Related object: L<ZBW::Ifis::Schema::Result::Sprache>

=cut

__PACKAGE__->belongs_to(
  "nr_sprache_relation",
  "ZBW::Ifis::Schema::Result::Sprache",
  { nr_sprache => "nr_sprache" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 pub_klass_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubKlass>

=cut

__PACKAGE__->has_many(
  "pub_klass_relations",
  "ZBW::Ifis::Schema::Result::PubKlass",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_plan_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubPlan>

=cut

__PACKAGE__->has_many(
  "pub_plan_relations",
  "ZBW::Ifis::Schema::Result::PubPlan",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_pubart_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubPubart>

=cut

__PACKAGE__->has_many(
  "pub_pubart_relations",
  "ZBW::Ifis::Schema::Result::PubPubart",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_quelle_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubQuelle>

=cut

__PACKAGE__->has_many(
  "pub_quelle_relations",
  "ZBW::Ifis::Schema::Result::PubQuelle",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_rel_nr_publikation_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubRel>

=cut

__PACKAGE__->has_many(
  "pub_rel_nr_publikation_relations",
  "ZBW::Ifis::Schema::Result::PubRel",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_rel_pub_nr_publikation_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubRel>

=cut

__PACKAGE__->has_many(
  "pub_rel_pub_nr_publikation_relations",
  "ZBW::Ifis::Schema::Result::PubRel",
  { "foreign.pub_nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_syn_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubSyn>

=cut

__PACKAGE__->has_many(
  "pub_syn_relations",
  "ZBW::Ifis::Schema::Result::PubSyn",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 pub_url_relations

Type: has_many

Related object: L<ZBW::Ifis::Schema::Result::PubUrl>

=cut

__PACKAGE__->has_many(
  "pub_url_relations",
  "ZBW::Ifis::Schema::Result::PubUrl",
  { "foreign.nr_publikation" => "self.nr_publikation" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FUGzwFnZCJoMQxWPJ8TioQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
