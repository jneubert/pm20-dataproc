use utf8;
package ZBW::Ifis::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-02-14 18:12:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:F7vAMzY8z67JOyzm9S11Hw

sub get_schema {
  my $self = shift;

  # requrires ~/.pgpass file to work
  # does not use ssh tunnel to wfmlabs bastion host - which is too slow - ,
  # but local replica
  #
  my $dsn       = 'dbi:Pg:dbname=ifis;host=localhost;port=5432';
  my $user      = 'root';
  my %conn_attr = (
    RaiseError     => 1,
    pg_enable_utf8 => 1,
  );

  my $schema = $self->connect( $dsn, $user, undef, \%conn_attr )
    ##or $log->logconfess("can't connect to $dsn");
    or die("can't connect to $dsn\n");
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
